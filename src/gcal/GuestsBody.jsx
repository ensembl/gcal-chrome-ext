import React, { useContext, useEffect, useRef } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import { AuthContext } from './AuthProvider.jsx';
import { ViewModeContext } from './ViewModeProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { GcalGuestProvider } from './GcalGuestProvider.jsx';
import AddGuests from './AddGuests.jsx';
import Guest from './Guest.jsx';

const GuestsBody = ({
  guests,
  setGuestsChanged,
  showBodies,
  setShowBodies,
}) => {
  const { authUserEmail } = useContext(AuthContext);
  const { gcalGuests } = useContext(GcalGuestsContext);
  const initialGcalGuestsState = useRef(JSON.stringify(gcalGuests));

  const { modalViewEvent } = useContext(ViewModeContext);
  let gcalGuestsShow = gcalGuests;
  if (modalViewEvent) {
    gcalGuestsShow = gcalGuests.filter((g) => g.email !== authUserEmail);
  }
  const showGuests = (
    (modalViewEvent && gcalGuestsShow.length > 0)
    || (!modalViewEvent && gcalGuestsShow.length > 1)
  );

  useEffect(() => {
    if (setGuestsChanged) {
      setGuestsChanged(JSON.stringify(gcalGuests) !== initialGcalGuestsState.current);
    }
  }, [JSON.stringify(gcalGuests)]);

  return (
    <>
      {!modalViewEvent && (
        <AddGuests />
      )}
      {showGuests && gcalGuestsShow.map((gcalGuest) => (
        <GcalGuestProvider
          key={gcalGuest.email}
          gcalGuest={gcalGuest}
        >
          <Box marginTop="5px">
            <Guest
              // guest may be undefined if it doesn't exist yet in Firestore
              guest={guests.find((g) => g.email === gcalGuest.email)}
              gcalGuestEmail={gcalGuest.email}
              showBodies={showBodies}
              setShowBodies={setShowBodies}
            />
          </Box>
        </GcalGuestProvider>
      ))}
    </>
  );
};

GuestsBody.propTypes = {
  guests: PropTypes.array.isRequired,
  setGuestsChanged: PropTypes.func.isReequired,
  showBodies: PropTypes.objectOf(PropTypes.any).isRequired,
  setShowBodies: PropTypes.func.isRequired,
};

export default GuestsBody;
