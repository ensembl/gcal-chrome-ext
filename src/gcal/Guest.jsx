import React, { useState, useEffect, useContext } from 'react';

import { Box, Card } from '@mui/material';
import PropTypes from 'prop-types';

import { ViewModeContext } from './ViewModeProvider.jsx';
import GuestHeader from './GuestHeader.jsx';
import GuestBody from './GuestBody.jsx';

const Guest = ({
  guest, // can be undefined, see GuestsBody.jsx
  gcalGuestEmail,
  showBodies,
  setShowBodies,
}) => {
  const { modalViewEvent } = useContext(ViewModeContext);

  const [showBody, setShowBody] = useState(true);

  const toggleShowBody = () => {
    const newShowBody = !showBody;
    setShowBody(newShowBody);
    if (modalViewEvent) {
      const newShowBodies = JSON.parse(JSON.stringify(showBodies));
      newShowBodies[gcalGuestEmail] = newShowBody;
      setShowBodies(newShowBodies);
    }
  };

  useEffect(() => {
    if (modalViewEvent) {
      setShowBody(showBodies[gcalGuestEmail]);
    }
  }, [showBodies]);

  return (
    <Card variant="outlined">
      <GuestHeader
        toggleShowBody={toggleShowBody}
        tasks={(guest && guest.tasks) || {}}
      />
      <Box
        sx={{ display: showBody ? 'block' : 'none' }}>
        <GuestBody
          reason={(guest && guest.reason) || ''}
          reasonInFirestore={!!guest && !!('reason' in guest)}
          tasks={(guest && guest.tasks) || {}}
          blockOffStarted={!!guest && !!guest.blockOffStarted}
          prep={(guest && guest.prep) || {}}
        />
      </Box>
    </Card>
  );
};

Guest.propTypes = {
  guest: PropTypes.objectOf(PropTypes.any).isRequired,
  gcalGuestEmail: PropTypes.string.isRequired,
  showBodies: PropTypes.objectOf(PropTypes.any).isRequired,
  setShowBodies: PropTypes.func.isRequired,
};

export default Guest;
