import React from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import TitleBodyEdit from './TitleBodyEdit.jsx';

const TitlePageView = ({ setTitleChanged }) => (
  <>
    <Box
      fontWeight="500"
      style={{
        fontSize: '1.4rem',
      }}
    >
      Title
    </Box>
    <Box marginTop="5px">
      <TitleBodyEdit setTitleChanged={setTitleChanged} />
    </Box>
  </>
);

TitlePageView.propTypes = {
  setTitleChanged: PropTypes.func.isRequired,
};

export default TitlePageView;
