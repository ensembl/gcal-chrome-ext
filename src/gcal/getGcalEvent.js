import { Calendar } from './Calendar';

const getGcalEvent = async ({ token, eventId }) => {
  const calendar = new Calendar({ token });
  const { result } = await calendar.getEvent({ eventId });
  return { result };
};

export { getGcalEvent };
