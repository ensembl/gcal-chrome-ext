import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Stack, Box, IconButton } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';

import { ViewModeContext } from './ViewModeProvider.jsx';

const GuestsHeader = ({
  showBodies,
  toggleShowBodies,
}) => {
  const { modalViewEvent } = useContext(ViewModeContext);

  const allShowing = Object.entries(showBodies).every(([, show]) => show);

  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="space-between"
      onClick={modalViewEvent ? toggleShowBodies : null}
      fontSize="1.4rem"
      style={{ cursor: modalViewEvent ? 'pointer' : 'auto' }}
    >
      <Box fontWeight="500">
        Guests
      </Box>
      {modalViewEvent && Object.keys(showBodies).length > 0 && (
        <IconButton
          title={allShowing ? 'Collapse all' : 'Expand all'}
          size="small"
          color="primary"
          onClick={toggleShowBodies}
          style={{ marginLeft: 'auto' }}
        >
          {allShowing ? <ExpandLessIcon fontSize="small" /> : <ExpandMoreIcon fontSize="small" />}
        </IconButton>
      )}
    </Stack>
  );
};

GuestsHeader.propTypes = {
  showBodies: PropTypes.bool.isRequired,
  toggleShowBodies: PropTypes.func.isRequired,
};

export default GuestsHeader;
