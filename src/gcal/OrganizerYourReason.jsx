import React, { useContext } from 'react';

import { Avatar, Box } from '@mui/material';
import PropTypes from 'prop-types';

import { GcalGuestsContext } from './GcalGuestsProvider.jsx';

const OrganizerYourReason = ({
  reason,
}) => {
  const { orgAvatarUrl } = useContext(GcalGuestsContext);

  const message = reason
    ? <>
      <span>invites you to: </span>
      <Box component="span" fontWeight="500">{reason}</Box>
    </>
    : <>
      <span>{' invites you without a reason. '}</span>
      < Box component="span" fontWeight="500">Ask them for one.</Box>
    </>;

  return (
    <>
      <Avatar
        style={{ height: '24px', width: '24px', display: 'inline-block' }}
        src={orgAvatarUrl}
      />
      <Box
        component="span"
        marginLeft="5px"
        style={{ verticalAlign: '37%' }}
      >
        {message}
      </Box>
    </>
  );
};

OrganizerYourReason.propTypes = {
  reason: PropTypes.string.isRequired,
};

export default OrganizerYourReason;
