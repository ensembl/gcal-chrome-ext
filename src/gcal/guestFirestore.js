import { firebase } from '../firebase-config';

const setReason = async ({
  guestsRef,
  email,
  reason,
}) => {
  if (guestsRef) {
    await guestsRef.doc(email).set(
      {
        reason,
      },
      { merge: true },
    );
  }
};

const setTaskText = async ({
  guestsRef,
  email,
  createdAt,
  text,
}) => {
  if (guestsRef) {
    await guestsRef.doc(email).set(
      {
        tasks: {
          [createdAt]: { text },
        },
      },
      { merge: true },
    );
  }
};

const setTaskState = async ({
  guestsRef,
  email,
  createdAt,
  state,
}) => {
  if (guestsRef) {
    await guestsRef.doc(email).set(
      {
        tasks: {
          [createdAt]: { state },
        },
      },
      { merge: true },
    );
  }
};

const deleteTask = async ({
  guestsRef,
  email,
  createdAt,
}) => {
  if (guestsRef) {
    await guestsRef.doc(email).set(
      {
        tasks: {
          [createdAt]: firebase.firestore.FieldValue.delete(),
        },
      },
      { merge: true },
    );
  }
};

const setPrep = async ({
  guestsRef,
  authUserEmail,
  prepEventId,
  prepHtmlLink,
  blockOffLength,
  startTimeString,
}) => {
  if (guestsRef) {
    await guestsRef.doc(authUserEmail).set(
      {
        prep: {
          eventId: prepEventId,
          htmlLink: prepHtmlLink,
          blockOffLength,
          startTimeString,
        },
      },
      { merge: true },
    );
  }
};

const setBlockOffStarted = async ({
  guestsRef,
  authUserEmail,
}) => {
  if (guestsRef) {
    await guestsRef.doc(authUserEmail).set(
      { blockOffStarted: true },
      { merge: true },
    );
  }
};

const setLiked = async ({
  guestsRef,
  authUserEmail,
  liked,
}) => {
  if (guestsRef) {
    await guestsRef.doc(authUserEmail).set(
      { liked },
      { merge: true },
    );
  }
};

export {
  setReason,
  setTaskText,
  setTaskState,
  deleteTask,
  setPrep,
  setBlockOffStarted,
  setLiked,
};
