import React from 'react';

import { Badge, Avatar } from '@mui/material';
import PropTypes from 'prop-types';

import EnsemblTootlip from './EnsemblTooltip.jsx';
import './GuestAvatar.css';

const GuestAvatar = ({
  avatarUrl,
  avatarSize,
  display,
  responseStatus,
  hasTooltip,
}) => {
  let tooltip = `${display} pending response`;
  if (responseStatus === 'accepted') {
    tooltip = `${display} accepted meeting`;
  } else if (responseStatus === 'declined') {
    tooltip = `${display} declined meeting`;
  } else if (responseStatus === 'tentative') {
    tooltip = `${display} maybe attending`;
  }

  const avatarWithBadge = (
    <Badge
      className={`EnsemblGuestAvatar EnsemblGuestAvatar__${responseStatus}`}
      overlap="circular"
      anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
      variant="dot"
      badgeContent={(responseStatus !== 'accepted') && (responseStatus !== 'declined') && (responseStatus !== 'tentative') ? 0 : 1} // 0 means no badge
    >
      <Avatar
        style={{ height: `${avatarSize}`, width: `${avatarSize}` }}
        src={avatarUrl}
      />
    </Badge>
  );

  return (
    <>
      {hasTooltip && (
        <EnsemblTootlip
          placement="bottom"
          title={tooltip}
          content={avatarWithBadge}
        />
      )}
      {!hasTooltip && avatarWithBadge}
    </>
  );
};

GuestAvatar.propTypes = {
  avatarUrl: PropTypes.string.isRequired,
  avatarSize: PropTypes.string.isRequired,
  display: PropTypes.string.isRequired,
  responseStatus: PropTypes.string.isRequired,
  hasTooltip: PropTypes.bool.isRequired,
};

export default GuestAvatar;
