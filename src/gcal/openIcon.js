/* eslint-disable no-param-reassign */

import { getOpenIcon, getAvatarDiv } from './htmlNodes';
import { setSidebarShow, setContainerVisibility } from './sidebar';

const attachOpenIcon = ({ openIcon }) => {
  const { openIcon: attachedOpenIcon } = getOpenIcon();
  if (!attachedOpenIcon) {
    const { avatarDiv } = getAvatarDiv();
    if (avatarDiv) {
      openIcon.addEventListener('click', () => {
        setSidebarShow({ show: true });
        setContainerVisibility({ visibility: 'visible' });
      });
      avatarDiv.appendChild(openIcon);
    }
  }
};

const detachOpenIcon = () => {
  const { openIcon: attachedOpenIcon } = getOpenIcon();
  if (attachedOpenIcon) {
    attachedOpenIcon.remove();
  }
};

export { attachOpenIcon, detachOpenIcon };
