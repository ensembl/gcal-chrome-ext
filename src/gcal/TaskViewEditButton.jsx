import React from 'react';

import { IconButton } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';
import PropTypes from 'prop-types';

const TaskViewEditButton = ({
  enterEditMode,
  disabled,
}) => (
  <IconButton
    size="small"
    title="Edit task"
    onClick={enterEditMode}
    disabled={disabled}
  >
    <EditIcon fontSize="1.2rem" />
  </IconButton>
);

TaskViewEditButton.propTypes = {
  enterEditMode: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

export default TaskViewEditButton;
