import React from 'react';

import { IconButton } from '@mui/material';
import PropTypes from 'prop-types';

import { taskStateOptions } from './taskStateOptions';

const TaskViewStateButton = ({
  state,
  enterChangeStateMode,
}) => {
  const option = taskStateOptions.find((o) => o.state === state);
  const icon = option ? option.icon : taskStateOptions[0].icon;

  return (
    <IconButton
      size="small"
      title="Change task state"
      onClick={enterChangeStateMode}
    >
      {icon}
    </IconButton>
  );
};

TaskViewStateButton.propTypes = {
  state: PropTypes.string.isRequired,
  enterChangeStateMode: PropTypes.func.isRequired,
};

export default TaskViewStateButton;
