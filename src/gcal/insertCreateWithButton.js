import {
  getCreateWithButtonAttached,
  clickMoreOptionsSpan,
  getSaveButtonDiv,
} from './htmlNodes';
import { setSidebarShow } from './sidebar';

const insertCreateWithButton = ({ createWithButton }) => {
  const { attached } = getCreateWithButtonAttached();
  if (!attached) {
    const { saveButtonDiv } = getSaveButtonDiv();

    if (saveButtonDiv) {
      createWithButton.addEventListener('click', () => {
        setSidebarShow({ show: true });
        clickMoreOptionsSpan();
      });
      saveButtonDiv.before(createWithButton);
    }
  }
};

export { insertCreateWithButton };
