import React from 'react';

import PropTypes from 'prop-types';
import { Stack, Box, Button } from '@mui/material';

const EditButtons = ({
  saveDisabled,
  clickSave,
  clickDelete,
  exitEditMode,
}) => (
  <Stack direction="row" alignItems="center">
    <Button
      type="button"
      variant="contained"
      color="primary"
      size="small"
      disableElevation
      disabled={saveDisabled}
      onClick={() => { exitEditMode(); clickSave(); }}
    >
      Save
    </Button>
    <Box marginLeft="5px">
      <Button
        type="button"
        variant="outlined"
        color="primary"
        size="small"
        disableElevation
        onClick={exitEditMode}
      >
        Cancel
      </Button>
    </Box>
    {clickDelete && (
      <Box marginLeft="auto">
        <Button
          type="button"
          variant="text"
          color="error"
          size="small"
          disableElevation
          onClick={() => { exitEditMode(); clickDelete(); }}
        >
          Delete
        </Button>
      </Box>
    )}
  </Stack >
);

EditButtons.propTypes = {
  saveDisabled: PropTypes.bool.isRequired,
  clickSave: PropTypes.func.isRequired,
  clickDelete: PropTypes.func,
  exitEditMode: PropTypes.func.isRequired,
};

EditButtons.defaultProps = {
  clickDelete: null,
};

export default EditButtons;
