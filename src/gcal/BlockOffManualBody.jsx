import React, { useContext, useState } from 'react';

import { Box } from '@mui/material';

import PropTypes from 'prop-types';
import { redirectToEvent } from './redirectToEvent';
import { AuthContext } from './AuthProvider.jsx';
import BlockOffFixedMins from './BlockOffFixedMins.jsx';
import BlockOffPrepare from './BlockOffPrepare.jsx';

const BlockOffManualBody = ({ blockOffStarted, prep }) => {
  const { authUserEmail } = useContext(AuthContext);

  const [visitingPrep, setVisitingPrep] = useState(false);
  const hasPrep = !!prep.blockOffLength;

  const redirectToPrep = () => {
    setVisitingPrep(true);
    const { eventId = '', htmlLink = '' } = prep;
    redirectToEvent({ eventId, htmlLink, email: authUserEmail });
  };

  return (
    <>
      {!hasPrep && (
        <Box marginBottom="8px">
          {[15, 30].map((blockOffLength) => (
            <BlockOffFixedMins
              key={blockOffLength}
              blockOffLength={blockOffLength}
              blockOffStarted={blockOffStarted}
            />
          ))}
        </Box>
      )}
      {hasPrep && (
        <BlockOffPrepare
          redirectToPrep={redirectToPrep}
          visitingPrep={visitingPrep}
          blockOffLength={prep.blockOffLength || ''}
          startTimeString={prep.startTimeString || ''}
        />
      )}
    </>
  );
};

BlockOffManualBody.propTypes = {
  blockOffStarted: PropTypes.bool.isRequired,
  prep: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default BlockOffManualBody;
