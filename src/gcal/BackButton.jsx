import React, { useContext } from 'react';

import { Button } from '@mui/material';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';

import { getModalViewEventCloseDiv, getPageCancelButtonDiv } from './htmlNodes';
import { ViewModeContext } from './ViewModeProvider.jsx';
import './BackButton.css';

const BackButton = () => {
  const { modalViewEvent } = useContext(ViewModeContext);

  const onClick = () => {
    if (modalViewEvent) {
      const { modalViewEventCloseDiv } = getModalViewEventCloseDiv();
      if (modalViewEventCloseDiv) {
        modalViewEventCloseDiv.click();
      }
    } else {
      const { cancelButtonDiv } = getPageCancelButtonDiv();
      if (cancelButtonDiv) {
        cancelButtonDiv.click();
      }
    }
  };

  return (
    <Button
      className="EnsemblBackButton"
      startIcon={<ChevronLeftIcon />}
      onClick={onClick}
      size="small"
    >
      Back to My meetings
    </Button>
  );
};

export default BackButton;
