import React, { useState } from 'react';

import {
  MenuItem, Select, FormControl, Stack,
} from '@mui/material';
import PropTypes from 'prop-types';

import { taskStateOptions } from './taskStateOptions';
import './TaskStateChange.css';

const TaskStateChange = ({
  state,
  exitChangeStateMode,
  setStateToFirestoreAndNotify,
}) => {
  const [open, setOpen] = useState(true);

  const onClose = () => {
    setOpen(false);
    exitChangeStateMode();
  };

  const onChange = (e) => {
    setOpen(false);
    const newState = e.target.value;
    if (newState !== state) {
      setStateToFirestoreAndNotify({ newState });
    }
    exitChangeStateMode();
  };

  return (
    <FormControl
      className="EnsemblTaskStateChange"
      size="small"
      fullWidth
    >
      <Select
        MenuProps={{ disablePortal: true }}
        open={open}
        onClose={onClose}
        onChange={onChange}
        displayEmpty
        value={state}
      >
        {taskStateOptions.map((option) => (
          <MenuItem
            key={option.state}
            value={option.state}
          >
            <Stack
              direction="row"
              alignItems="center"
              spacing="10px"
              style={{ fontSize: '0.9rem' }}
            >
              {option.icon}
              <span>{option.display}</span>
            </Stack>
          </MenuItem>
        ))}
      </Select>
    </FormControl >
  );
};

TaskStateChange.propTypes = {
  state: PropTypes.string.isRequired,
  exitChangeStateMode: PropTypes.func.isRequired,
  setStateToFirestoreAndNotify: PropTypes.func.isRequired,
};

export default TaskStateChange;
