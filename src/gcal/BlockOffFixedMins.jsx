import React, { useContext, useState } from 'react';

import PropTypes from 'prop-types';
import {
  Stack, Button, Box, CircularProgress,
} from '@mui/material';
import { GcalEventContext } from './GcalEventProvider.jsx';
import { createPrepEvent } from './createPrepEvent';

const BlockOffFixedMins = ({
  blockOffLength,
  blockOffStarted,
}) => {
  const { gcalEventId } = useContext(GcalEventContext);

  const [waiting, setWaiting] = useState(false);

  const onClick = () => {
    setWaiting(true);
    createPrepEvent({
      eventId: gcalEventId,
      blockOffLength,
    });
  };

  return (
    <Box marginTop="10px">
      <Button
        size="small"
        onClick={onClick}
        variant="outlined"
        color="primary"
        disabled={waiting || blockOffStarted}
        fullWidth
        disableElevation
      >
        {waiting
          ? (
            <Stack direction="row" alignItems="center">
              <CircularProgress color="inherit" size={14} />
              <Box marginLeft="7px">{`Blocking off ${blockOffLength} minutes...`}</Box>
            </Stack>
          )
          : <span>{`Block off ${blockOffLength} minutes`}</span>
        }
      </Button>
    </Box>
  );
};

BlockOffFixedMins.propTypes = {
  blockOffLength: PropTypes.number.isRequired,
  blockOffStarted: PropTypes.bool.isRequired,
};

export default BlockOffFixedMins;
