import React, { useState, useContext } from 'react';

import {
  Stack, Button, CircularProgress, Box,
} from '@mui/material';

import { getCreateButton } from './htmlNodes';
import { ViewModeContext } from './ViewModeProvider.jsx';

const MyMeetingsHeader = () => {
  const { modalNewEvent } = useContext(ViewModeContext);

  const [waiting, setWaiting] = useState(false);

  const onClick = () => {
    setWaiting(true);
    const { createButton } = getCreateButton();
    if (!modalNewEvent && createButton) {
      localStorage.setItem('meetings.ensembl.so/new-meeting', 'yes');
      createButton.click();
    } else {
      window.location.href = 'https://calendar.google.com/calendar/r/eventedit';
    }
  };

  return (
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="space-between"
    >
      <h2 style={{ fontSize: '1.6rem', margin: 0 }}>
        My meetings
      </h2>
      <Button
        variant="contained"
        size="small"
        disableElevation
        onClick={onClick}
        disabled={waiting}
      >
        {waiting
          ? (
            <Stack direction="row" alignItems="center" justifyContent="center">
              <CircularProgress color="inherit" size={14} />
              <Box marginLeft="7px">New</Box>
            </Stack>
          )
          : 'New'
        }
      </Button>
    </Stack>
  );
};

export default MyMeetingsHeader;
