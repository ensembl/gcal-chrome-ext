import React from 'react';

import PropTypes from 'prop-types';

export const GcalGuestContext = React.createContext();

export const GcalGuestProvider = ({
  gcalGuest,
  children,
}) => (
  <GcalGuestContext.Provider
    value={{
      email: gcalGuest.email || '',
      display: gcalGuest.display || '',
      avatarUrl: gcalGuest.avatarUrl || '',
      organizer: gcalGuest.organizer,
      responseStatus: gcalGuest.responseStatus,
    }}
  >
    {children}
  </GcalGuestContext.Provider>
);

GcalGuestProvider.propTypes = {
  gcalGuest: PropTypes.objectOf(PropTypes.any).isRequired,
  children: PropTypes.node.isRequired,
};

export default GcalGuestProvider;
