import { getEventChipDiv } from './htmlNodes';

const redirectToEvent = ({ eventId, email, htmlLink }) => {
  // https://stackoverflow.com/questions/6916805/why-does-a-base64-encoded-string-have-an-sign-at-the-end
  const dataEventId = window.btoa(`${eventId} ${email}`).replace(/=/g, '');
  const { eventChipDiv } = getEventChipDiv({ dataEventId });
  if (eventChipDiv) {
    eventChipDiv.click();
  } else {
    window.location.href = htmlLink || 'https://calendar.google.com/calendar/';
  }
};

export { redirectToEvent };
