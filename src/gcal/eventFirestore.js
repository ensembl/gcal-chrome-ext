const addEventIfNeeded = async ({
  eventRef,
  createdByUid,
  createdByEmail,
  createdByDomain,
  authUserName,
}) => {
  const eventDoc = await eventRef.get();
  if (!eventDoc.exists) {
    await eventRef.set({
      createdByUid,
      createdByEmail,
      createdByDomain,
      createdByName: authUserName,
      createdAt: new Date(),
      gcalGuestEmails: [createdByEmail],
    });
  }
};

const setGcalGuestEmails = async ({
  eventRef,
  gcalGuestEmails,
}) => {
  if (eventRef) {
    await eventRef.update({ gcalGuestEmails });
  }
};

const addPrepEvent = async ({
  eventsRef,
  prepEventId,
  origEventId,
  createdByUid,
  createdByEmail,
  createdByDomain,
}) => {
  await eventsRef.doc(prepEventId).set({
    isPrep: true,
    origEventId,
    createdByUid,
    createdByEmail,
    createdByDomain,
    createdAt: new Date(),
  }, { merge: true });
};

const setGcalEventInfoFirestore = async ({
  eventRef,
  title,
  startTime,
  updatedAt,
}) => {
  if (eventRef) {
    await eventRef.set({
      title,
      startTime,
      updatedAt,
    }, { merge: true });
  }
};

export {
  addEventIfNeeded,
  setGcalGuestEmails,
  addPrepEvent,
  setGcalEventInfoFirestore,
};
