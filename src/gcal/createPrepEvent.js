import { db, firebase } from '../firebase-config';
import { setBlockOffStarted, setPrep } from './guestFirestore';
import { addPrepEvent } from './eventFirestore';

const { runtime } = chrome; /* global chrome */

const createPrepEvent = async ({
  eventId,
  blockOffLength,
}) => {
  const auth = firebase.auth();
  const { uid: authUid, email: authUserEmail } = auth.currentUser;
  const guestsRef = db.collection(`events/${eventId}/guests`);

  await setBlockOffStarted({ guestsRef, authUserEmail });
  runtime.sendMessage(
    {
      msg: 'create-prep-gcal-event',
      payload: {
        eventId,
        blockOffLength,
      },
    },
    async (response) => {
      const {
        noPrepEventInserted = false,
        prepEventId = '',
        prepHtmlLink = '',
        prepStartTimeString = '',
      } = response;
      if (noPrepEventInserted) { return; }

      await addPrepEvent({
        eventsRef: db.collection('events'),
        prepEventId,
        origEventId: eventId,
        createdByUid: authUid,
        createdByEmail: authUserEmail,
        createdByDomain: authUserEmail.slice(authUserEmail.indexOf('@') + 1),
      });
      await setPrep({
        guestsRef,
        authUserEmail,
        prepEventId,
        prepHtmlLink,
        blockOffLength,
        startTimeString: prepStartTimeString,
      });
    },
  );
};

export { createPrepEvent };
