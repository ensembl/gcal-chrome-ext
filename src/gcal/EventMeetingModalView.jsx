import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import { AuthContext } from './AuthProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import TitleModalView from './TitleModalView.jsx';
import RecurringWarning from './RecurringWarning.jsx';
import Likes from './Likes.jsx';
import EventMeetingModalViewAttendee from './EventMeetingModalViewAttendee.jsx';
import EventMeetingModalViewOrganizer from './EventMeetingModalViewOrganizer.jsx';

const EventMeetingModalView = ({
  recurring,
  guests,
}) => {
  const { orgEmail, gcalGuests } = useContext(GcalGuestsContext);
  const { authUserEmail } = useContext(AuthContext);

  const hasGuests = gcalGuests.length > 0;
  const isOrganizer = orgEmail === authUserEmail;

  return (
    <>
      <Box marginBottom="15px">
        <TitleModalView />
      </Box>
      {recurring && (
        <Box marginBottom="15px">
          <RecurringWarning />
        </Box>
      )}
      {hasGuests && (
        <>
          <Box marginBottom="15px">
            <Likes
              guests={guests}
            />
          </Box>
          {!isOrganizer && (
            <EventMeetingModalViewAttendee
              guests={guests}
            />
          )}
          {isOrganizer && (
            <EventMeetingModalViewOrganizer
              guests={guests}
            />
          )}
        </>
      )}
    </>
  );
};

EventMeetingModalView.propTypes = {
  recurring: PropTypes.bool.isRequired,
  guests: PropTypes.array.isRequired,
};

export default EventMeetingModalView;
