import React, { useState, useContext } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import { ViewModeContext } from './ViewModeProvider.jsx';
import TitlePageView from './TitlePageView.jsx';
import RecurringUnsupported from './RecurringUnsupported.jsx';
import RecurringWarning from './RecurringWarning.jsx';
import Guests from './Guests.jsx';
import SaveMeetingButton from './SaveMeetingButton.jsx';

const EventMeetingPageView = ({
  recurring,
  guests,
}) => {
  const [titleChanged, setTitleChanged] = useState(false);
  const [guestsChanged, setGuestsChanged] = useState(false);
  const { autoSave } = useContext(ViewModeContext);
  const recurringAutoSave = recurring && autoSave;

  return (
    <>
      {recurringAutoSave && (
        <Box marginTop="15px">
          <RecurringUnsupported />
        </Box>
      )}
      {!recurringAutoSave && (
        <>
          <Box marginBottom="15px">
            <TitlePageView
              setTitleChanged={setTitleChanged}
            />
          </Box>
          {recurring && (
            <Box marginBottom="15px">
              <RecurringWarning />
            </Box>
          )}
          <Box marginBottom="15px">
            <Guests
              guests={guests}
              setGuestsChanged={setGuestsChanged}
            />
          </Box>
          <SaveMeetingButton
            disabled={(!titleChanged && !guestsChanged) && !autoSave}
          />
        </>
      )}
    </>
  );
};

EventMeetingPageView.propTypes = {
  recurring: PropTypes.bool.isRequired,
  guests: PropTypes.array.isRequired,
};

export default EventMeetingPageView;
