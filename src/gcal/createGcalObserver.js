/* eslint-disable no-param-reassign */
import React from 'react';

import ReactDOM from 'react-dom';

import {
  getDescriptionDiv,
  getGcalEventIdOnPageEvent,
  getGcalEventIdOnModalViewEvent,
  getOptionsDiv,
  getModalViewEventDiv,
  getDynamicContainerAttached,
  getDockToSidebarDiv,
  getGcalDiv,
  getTitleInputValue,
  getUserEmail,
  getRecurringOnPage,
  clickMoreOptionsSpan,
} from './htmlNodes';
import { insertCreateWithButton } from './insertCreateWithButton';
import { attachOpenButton } from './attachOpenButton';
import { attachOpenIcon, detachOpenIcon } from './openIcon';
import { getGcalGuests } from './getGcalGuests';
import { getSidebarShow, setContainerVisibility } from './sidebar';
import { notifyNewEvent } from './notifyNewEvent';
import { createPrepEvent } from './createPrepEvent';

import App from './App.jsx';

const { storage } = chrome; /* global chrome */

const getHasValidMutations = ({ mutations, dynamicContainer }) => {
  const { descriptionDiv } = getDescriptionDiv();

  const invalidMutation = mutations.find((mutation) => {
    const { target } = mutation;
    return (
      (dynamicContainer.contains(target))
      || (descriptionDiv && descriptionDiv.contains(target))
    );
  });

  if (invalidMutation) {
    return { hasValidMutations: false };
  }

  return { hasValidMutations: true };
};

const getEventModes = () => {
  const splits = window.location.href.split('eventedit');
  const { optionsDiv } = getOptionsDiv();
  const { dockToSidebarDiv } = getDockToSidebarDiv();
  const pageNewEvent = splits.length > 1 && !splits[1].startsWith('/');
  const pageEditEvent = splits.length > 1 && splits[1].startsWith('/');
  const modalViewEvent = !!optionsDiv;
  return {
    pageNewEvent,
    pageEditEvent,
    modalViewEvent,
    modalNewEvent: !!dockToSidebarDiv,
    calendarView: !pageNewEvent && !pageEditEvent && !modalViewEvent,
  };
};

const redirectToNewMeeting = ({ modalNewEvent }) => {
  if (
    modalNewEvent
    && (localStorage.getItem('meetings.ensembl.so/new-meeting') === 'yes')
  ) {
    localStorage.setItem('meetings.ensembl.so/new-meeting', 'no');
    clickMoreOptionsSpan();
    return { redirect: true };
  }
  return { redirect: false };
};

const updateContainers = ({
  dynamicContainer,
  pageNewEvent,
  pageEditEvent,
  modalViewEvent,
  calendarView,
}) => {
  // Attach the container and render the React app even if getSidebarShow() is false
  // since when the user shows the sidebar, the content should be immediately available.
  if (pageNewEvent || pageEditEvent || calendarView) {
    const { gcalDiv } = getGcalDiv();
    const { attached } = getDynamicContainerAttached();
    if (!attached && gcalDiv) {
      gcalDiv.after(dynamicContainer);
    }
  } else if (modalViewEvent) {
    const { modalViewEventDiv } = getModalViewEventDiv();
    // If container not attached to current modal (could be attached to another modal), attach it
    const { attached } = getDynamicContainerAttached({ modalViewEventDiv });
    if (!attached && modalViewEventDiv) {
      modalViewEventDiv.appendChild(dynamicContainer);
    }
  }
  const { show } = getSidebarShow();
  setContainerVisibility({ visibility: show ? 'visible' : 'hidden' });
};

const getGcalEventId = ({ pageNewEvent, pageEditEvent, modalViewEvent }) => {
  let gcalEventId;
  if (pageNewEvent || pageEditEvent) {
    ({ gcalEventId } = getGcalEventIdOnPageEvent({ newEvent: pageNewEvent }));
  } else if (modalViewEvent) {
    ({ gcalEventId } = getGcalEventIdOnModalViewEvent());
  }
  return { gcalEventId };
};

const handleNewEvent = async ({ prevGcalEventId }) => {
  setTimeout(() => {
    notifyNewEvent({ newEventId: prevGcalEventId });
    const key = 'meetings.ensembl.so/block-off-length';
    storage.local.get([key], (result) => {
      const blockOffLength = result[key];
      if (['15', '30'].includes(blockOffLength)) {
        createPrepEvent({
          eventId: prevGcalEventId,
          blockOffLength,
        });
      }
    });
  }, 1000);
};

const updateControls = ({
  modalNewEvent, modalViewEvent, createWithButton, openButton, openIcon,
}) => {
  if (modalNewEvent) {
    insertCreateWithButton({ createWithButton });
  }
  if (modalViewEvent) {
    attachOpenButton({ openButton });
    detachOpenIcon();
  }
  if (!modalViewEvent) {
    attachOpenIcon({ openIcon });
  }
};

const createGcalObserver = ({
  dynamicContainer,
  createWithButton,
  openButton,
  openIcon,
}) => {
  let prevPageNewEvent;
  let prevGcalEventId;

  const mutationObserver = new MutationObserver((mutations) => {
    const { hasValidMutations } = getHasValidMutations({
      mutations,
      dynamicContainer,
    });
    if (!hasValidMutations) { return; }

    const {
      pageNewEvent, pageEditEvent, modalViewEvent, modalNewEvent, calendarView,
    } = getEventModes();

    const { redirect } = redirectToNewMeeting({ modalNewEvent });
    if (redirect) { return; }

    const { gcalEventId } = getGcalEventId({ pageNewEvent, pageEditEvent, modalViewEvent });
    const { gcalGuests } = getGcalGuests();

    updateContainers({
      dynamicContainer,
      pageNewEvent,
      pageEditEvent,
      modalViewEvent,
      calendarView,
    });

    if (gcalEventId !== prevGcalEventId) {
      ReactDOM.unmountComponentAtNode(dynamicContainer);
      if (prevPageNewEvent && prevGcalEventId) {
        handleNewEvent({ prevGcalEventId });
      }
    }

    prevPageNewEvent = pageNewEvent;
    prevGcalEventId = gcalEventId;

    if (pageNewEvent || pageEditEvent || modalViewEvent || calendarView) {
      ReactDOM.render(
        <App
          gcalEventId={gcalEventId}
          flatGcalGuests={JSON.stringify(gcalGuests)}
          autoSave={pageNewEvent}
          modalViewEvent={modalViewEvent}
          modalNewEvent={modalNewEvent}
          calendarView={calendarView}
          titleInputValue={(getTitleInputValue()).titleInputValue}
          userHtmlEmail={(getUserEmail()).userEmail}
          pageViewRecurring={(pageNewEvent || pageEditEvent) && (getRecurringOnPage()).recurring}
        />,
        dynamicContainer,
      );
    }

    updateControls({
      modalNewEvent, modalViewEvent, createWithButton, openButton, openIcon,
    });
  });
  mutationObserver.observe(document.body, { subtree: true, childList: true });
};

export { createGcalObserver };
