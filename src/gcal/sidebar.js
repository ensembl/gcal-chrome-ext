import {
  getBaseContainer, getDynamicContainer, getOpenButton,
} from './htmlNodes';

const setContainerVisibility = ({ visibility }) => {
  const { baseContainer } = getBaseContainer();
  if (baseContainer) {
    baseContainer.classList.add(visibility === 'visible' ? 'EnsemblVisible' : 'EnsemblHidden');
    baseContainer.classList.remove(visibility === 'visible' ? 'EnsemblHidden' : 'EnsemblVisible');
  }
  const { dynamicContainer } = getDynamicContainer();
  if (dynamicContainer) {
    dynamicContainer.classList.add(visibility === 'visible' ? 'EnsemblVisible' : 'EnsemblHidden');
    dynamicContainer.classList.remove(visibility === 'visible' ? 'EnsemblHidden' : 'EnsemblVisible');
  }
  const { openButton } = getOpenButton();
  if (openButton) {
    openButton.disabled = visibility === 'visible';
  }
};

const setSidebarShow = ({ show }) => {
  localStorage.setItem('meetings.ensembl.so/sidebar-show', show ? 'yes' : 'no');
};

// Show the sidebar if no setting is in local storage yet
const getSidebarShow = () => ({ show: !(localStorage.getItem('meetings.ensembl.so/sidebar-show') === 'no') });

export {
  setContainerVisibility,
  setSidebarShow,
  getSidebarShow,
};
