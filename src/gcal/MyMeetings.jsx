/* global chrome */

import React, { useEffect, useState } from 'react';

import { Box } from '@mui/material';
import PropTypes from 'prop-types';

import MyMeetingsHeader from './MyMeetingsHeader.jsx';
import MyMeetingsDayEvents from './MyMeetingsDayEvents.jsx';

const { runtime } = chrome;

const getDateTimeDisplays = ({
  startDate, startTime, endDate, endTime,
}) => {
  const startDateDisplay = startDate
    ? new Date(`${startDate}:00:00:00`).toLocaleDateString('en-US', { month: 'short', day: 'numeric' })
    : new Date(startTime).toLocaleDateString('en-US', { month: 'short', day: 'numeric' });
  let startDateGroupDisplay = startDate
    ? new Date(`${startDate}:00:00:00`).toLocaleDateString('en-US', { weekday: 'short', month: 'short', day: 'numeric' })
    : new Date(startTime).toLocaleDateString('en-US', { weekday: 'short', month: 'short', day: 'numeric' });
  const startTimeDisplay = startTime
    ? new Date(startTime).toLocaleTimeString('en-US', { hour12: true, hour: 'numeric', minute: 'numeric' })
    : '';

  const today = new Date();
  const todayDisplay = today.toLocaleDateString('en-US', { weekday: 'short', month: 'short', day: 'numeric' });

  const tomorrow = new Date();
  tomorrow.setDate(today.getDate() + 1);
  const tomorrowDisplay = tomorrow.toLocaleDateString('en-US', { weekday: 'short', month: 'short', day: 'numeric' });

  if (startDateGroupDisplay === todayDisplay) {
    startDateGroupDisplay = 'Today';
  }
  if (startDateGroupDisplay === tomorrowDisplay) {
    startDateGroupDisplay = 'Tomorrow';
  }

  const endDateDisplay = endDate
    ? new Date(new Date(`${endDate}:00:00:00`) - 1).toLocaleDateString('en-US', { month: 'short', day: 'numeric' }) // API end.date is event exclusive
    : new Date(endTime).toLocaleDateString('en-US', { month: 'short', day: 'numeric' });
  const endTimeDisplay = endTime
    ? new Date(endTime).toLocaleTimeString('en-US', { hour12: true, hour: 'numeric', minute: 'numeric' })
    : '';

  return {
    startDateDisplay, startDateGroupDisplay, startTimeDisplay, endDateDisplay, endTimeDisplay,
  };
};

const MyMeetings = ({ setMyMeetingsReady }) => {
  const [eventsByDay, setEventsByDay] = useState({});

  const getEventsByDay = ({ events }) => {
    const startDateGroupDisplays = [];
    const newEventsByDay = [];
    let index = -1;
    events.forEach((event) => {
      if (event.start && event.end) {
        const startDate = event.start.date || '';
        const startTime = event.start.dateTime || '';

        const endDate = event.end.date || '';
        const endTime = event.end.dateTime || '';

        const {
          startDateDisplay, startDateGroupDisplay, startTimeDisplay, endDateDisplay, endTimeDisplay,
        } = getDateTimeDisplays({
          startDate, startTime, endDate, endTime,
        });

        if (!startDateGroupDisplays.includes(startDateGroupDisplay)) {
          startDateGroupDisplays.push(startDateGroupDisplay);
          index += 1;
          newEventsByDay.push({
            startDateGroupDisplay,
            events: [],
          });
        }
        newEventsByDay[index].events.push({
          startDateDisplay,
          startTimeDisplay,
          endDateDisplay,
          endTimeDisplay,
          id: event.id || '',
          title: event.summary || '(No title)',
          htmlLink: event.htmlLink || '',
          attendees: event.attendees || [],
        });
      }
    });
    return { newEventsByDay };
  };

  useEffect(() => {
    runtime.sendMessage(
      { msg: 'get-gcal-events' },
      (response) => {
        if (response.events) {
          const { newEventsByDay } = getEventsByDay({ events: response.events });
          setEventsByDay(newEventsByDay);
        }
        setMyMeetingsReady(true);
      },
    );
  }, []);

  return (
    <>
      <MyMeetingsHeader />
      {eventsByDay.length > 0 && eventsByDay.map((dayEvents) => (
        <Box
          key={dayEvents.startDateDisplay}
          marginTop="10px"
        >
          <MyMeetingsDayEvents
            startDateGroupDisplay={dayEvents.startDateGroupDisplay || ''}
            events={dayEvents.events || []}
          />
        </Box>
      ))}
    </>
  );
};

MyMeetings.propTypes = {
  setMyMeetingsReady: PropTypes.func.isRequired,
};

export default MyMeetings;
