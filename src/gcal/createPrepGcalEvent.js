import { Calendar } from './Calendar';

const getPrepEventTime = async ({
  calendar,
  formattedEventStartTime,
  blockOffLength,
  userEmail,
}) => {
  /*
  * Set a proposed prep event time, check with FreeBusy API and make sure the
  * user is not busy during that time.  If the user is busy, go back 15 minutes
  * in time and check again.
  * TODO: Check the beginning of day and move the previous day
  * TODO: Check NOW and make sure prep event is not before now.
  */
  let proposed;

  const eventStartTime = new Date(formattedEventStartTime.getTime());
  const earlistPrepTime = new Date(formattedEventStartTime.getTime());
  // Search for 12 hours before the actual event
  earlistPrepTime.setHours(earlistPrepTime.getHours() - 12);

  const blockOffLengthMilliseconds = blockOffLength * 60000;

  const { result } = await calendar.getFreeBusy(
    {
      email: userEmail,
      timeMax: eventStartTime,
      timeMin: earlistPrepTime,
    },
  );
  // Can't find any free busy events
  if (result.calendars[userEmail].busy.length === 0) {
    // return the time slot immediate before the event
    proposed = {
      prepEventStartTime: new Date(eventStartTime - blockOffLengthMilliseconds),
      prepEventEndTime: eventStartTime,
    };
  } else {
    const freeBusyArray = result.calendars[userEmail].busy;

    const converted = freeBusyArray.map((interval) => ({
      start: new Date(interval.start),
      end: new Date(interval.end),
    }));
    converted.reverse();
    converted.unshift({ start: eventStartTime });

    for (let i = 0; i < converted.length; i += 1) {
      // if it's the last spot or found a slot in between busy slots
      if (converted.length - i === 1
        || converted[i].start - converted[i + 1].end >= blockOffLengthMilliseconds) {
        proposed = {
          prepEventStartTime: new Date(converted[i].start - blockOffLengthMilliseconds),
          prepEventEndTime: converted[i].start,
        };
        break;
      }
    }
  }

  return proposed;
};

const insertPrepGcalEvent = async ({
  calendar,
  startTime,
  endTime,
  origTitle,
  origHtmlLink,
}) => {
  const { result } = await calendar.insertEvent({
    startTime,
    endTime,
    title: `Prepare for: ${origTitle}`,
    description: `<span>⏳ Use this time to prepare for <a href="${origHtmlLink}">${origTitle}</span>.`,
  });
  return { prepEvent: result };
};

const createPrepGcalEvent = async ({
  token,
  eventId,
  blockOffLength,
}) => {
  const calendar = new Calendar({ token });

  const { result: origEvent } = await calendar.getEvent({ eventId });
  const {
    htmlLink: origHtmlLink = '',
    start: origStart = '',
    summary: origSummary = '',
    status = '',
    recurrence = [],
    attendees = [],
  } = origEvent;

  if (
    (status !== 'confirmed') // If user clicks undo, status is not confirmed
    || (recurrence.length > 0)
    || attendees.length === 0 // No guests
  ) {
    return {
      response: {
        noPrepEventInserted: true,
      },
    };
  }

  const { prepEventStartTime, prepEventEndTime } = await getPrepEventTime({
    calendar,
    formattedEventStartTime: new Date(origStart.dateTime),
    blockOffLength,
    userEmail: 'primary', // Block off time in user's primary calendar for now
  });

  const { prepEvent } = await insertPrepGcalEvent({
    calendar,
    startTime: prepEventStartTime,
    endTime: prepEventEndTime,
    origTitle: origSummary || '(No title)',
    origHtmlLink,
  });

  return {
    response: {
      prepEventId: prepEvent.id,
      prepHtmlLink: prepEvent.htmlLink,
      prepStartTimeString: prepEvent.start.dateTime,
    },
  };
};

export { createPrepGcalEvent };
