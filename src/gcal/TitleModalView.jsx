import React, { useContext } from 'react';

import { Box, IconButton } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';

import { getEditEventDiv } from './htmlNodes';
import { GcalEventContext } from './GcalEventProvider.jsx';

const TitleModalView = () => {
  const { title } = useContext(GcalEventContext);

  const onClick = () => {
    const { editEventDiv } = getEditEventDiv();
    if (editEventDiv) {
      editEventDiv.click();
    }
  };

  return (
    <Box>
      <h2 style={{ fontSize: '1.6rem', margin: 0, display: 'inline' }}>
        {title}
      </h2>
      <IconButton
        style={{ verticalAlign: 'bottom', marginLeft: '5px' }}
        size="small"
        title="Edit title and change guests"
        onClick={onClick}
      >
        <EditIcon fontSize="small" />
      </IconButton>
    </Box>
  );
};

export default TitleModalView;
