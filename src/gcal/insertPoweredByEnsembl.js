import { getAddDescriptionSpan, getDescriptionDiv } from './htmlNodes';

const removeEnsemblSection = ({ descriptionDiv }) => {
  descriptionDiv.querySelectorAll('div').forEach((spanNode) => {
    const { childNodes } = spanNode;
    if (childNodes.length > 0 && childNodes[0].textContent.includes('Powered by Ensembl')) {
      spanNode.parentNode.remove();
    }
  });
};

const insertPoweredByEnsembl = () => {
  // Click Add description programmatically in modal add event mode to activate description area
  const { addDescriptionSpan } = getAddDescriptionSpan();
  if (addDescriptionSpan) { addDescriptionSpan.click(); }

  // Bubble the keyup event so that an existing event listener catches it, telling the page that a
  // user typed in the field
  // This is necessary for programmatically inserted content there to be persisted
  // https://stackoverflow.com/questions/64094461/edit-descrption-or-location-of-google-calendar-event-with-chrome-extension
  const { descriptionDiv } = getDescriptionDiv();
  if (descriptionDiv) {
    descriptionDiv.dispatchEvent(new KeyboardEvent('keyup', { bubbles: true }));
    removeEnsemblSection({ descriptionDiv });
    const ensemblDiv = document.createElement('div');
    ensemblDiv.innerHTML = `
      <div>⚡ Powered by Ensembl</div>
      <div>Install the <a href="https://chrome.google.com/webstore/detail/ensembl-meeting-prep/mibamnjffhmadmdfoidoakkceppinbpa">Ensembl Chrome extension</a> to see all guest reasons and tasks.</div>
    `;
    descriptionDiv.appendChild(ensemblDiv);
  }
};

export { insertPoweredByEnsembl };
