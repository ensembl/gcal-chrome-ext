import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Stack } from '@mui/material';

import { setLiked } from './guestFirestore';
import { FirestoreRefsContext } from './FirestoreRefsProvider.jsx';
import { AuthContext } from './AuthProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import LikeButton from './LikeButton.jsx';
import LikeAvatars from './LikeAvatars.jsx';

const Likes = ({
  guests,
}) => {
  const { guestsRef } = useContext(FirestoreRefsContext);
  const { gcalGuests } = useContext(GcalGuestsContext);
  const { authUserEmail } = useContext(AuthContext);

  const likedGcalGuests = gcalGuests.filter((gcalGuest) => {
    const likedGuest = guests.find((guest) => guest.email === gcalGuest.email && guest.liked);
    return !!likedGuest;
  });

  // authGuest may be undefined if it doesn't exist yet in Firestore
  const authGuest = guests.find((g) => g.email === authUserEmail);
  const liked = authGuest && authGuest.liked;

  const setLikedToFirestore = async () => {
    await setLiked({
      guestsRef,
      authUserEmail,
      liked: !liked,
    });
  };

  return (
    <Stack
      direction="row"
      alignItems="center"
      spacing="12px"
    >
      <LikeButton
        progress={(likedGcalGuests.length / gcalGuests.length) * 100}
        liked={liked}
        setLikedToFirestore={setLikedToFirestore}
      />
      {likedGcalGuests.length > 0 && (
        <LikeAvatars
          likedGcalGuests={likedGcalGuests}
        />
      )}
    </Stack>
  );
};

Likes.propTypes = {
  guests: PropTypes.array.isRequired,
};

export default Likes;
