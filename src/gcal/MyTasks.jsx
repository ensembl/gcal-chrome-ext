import React from 'react';
import { Box, Card } from '@mui/material';
import PropTypes from 'prop-types';

import BlockOff from './BlockOff.jsx';
import Tasks from './Tasks.jsx';

const MyTasks = ({
  tasks,
  blockOffStarted,
  prep,
}) => (
  <>
    <Box
      fontWeight="500"
      fontSize="1.4rem"
      marginBottom="5px"
    >
      My tasks
    </Box>
    <Card
      variant="outlined"
      style={{ padding: '10px 15px' }}
    >
      <Tasks
        tasks={tasks}
        noHeader
      />
      <Box marginTop="10px">
        <BlockOff
          auto={false}
          blockOffStarted={blockOffStarted}
          prep={prep}
        />
      </Box>
    </Card>
  </>
);

MyTasks.propTypes = {
  tasks: PropTypes.objectOf(PropTypes.any).isRequired,
  blockOffStarted: PropTypes.bool.isRequired,
  prep: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default MyTasks;
