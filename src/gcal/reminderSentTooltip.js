import React from 'react';

import { Stack, Box } from '@mui/material';
import MarkedEmailOutlinedIcon from '@mui/icons-material/MarkEmailReadOutlined';

const reminderSentTooltip = (
  <Stack
    direction="row"
    alignItems="center"
    justifyContent="center"
    spacing="3px"
  >
    <MarkedEmailOutlinedIcon fontSize="small" style={{ color: 'rgb(76, 175, 80)' }} />
    <Box fontWeight="500">Reminder sent</Box>
  </Stack>
);

export { reminderSentTooltip };
