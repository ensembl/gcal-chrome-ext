import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import { ViewModeContext } from './ViewModeProvider.jsx';
import { FirestoreRefsProvider } from './FirestoreRefsProvider.jsx';
import { GcalEventProvider } from './GcalEventProvider.jsx';
import { GcalGuestsProvider } from './GcalGuestsProvider.jsx';
import EventMeetingModalView from './EventMeetingModalView.jsx';
import EventMeetingPageView from './EventMeetingPageView.jsx';

const EventMeeting = ({
  eventsRef,
  gcalEventId,
  eventRef,
  guestsRef,
  gcalGuests,
  title,
  titleInputValue,
  htmlLink,
  recurring,
  guests,
}) => {
  const { modalViewEvent } = useContext(ViewModeContext);
  return (
    <FirestoreRefsProvider
      eventsRef={eventsRef}
      eventRef={eventRef}
      guestsRef={guestsRef}
    >
      <GcalGuestsProvider
        gcalGuests={gcalGuests}
      >
        <GcalEventProvider
          gcalEventId={gcalEventId}
          title={title}
          titleInputValue={titleInputValue}
          htmlLink={htmlLink}
        >
          {modalViewEvent && (
            <EventMeetingModalView
              recurring={recurring}
              guests={guests}
            />
          )}
          {!modalViewEvent && (
            <EventMeetingPageView
              recurring={recurring}
              guests={guests}
            />
          )}
          <Box marginBottom="400px" />
        </GcalEventProvider>
      </GcalGuestsProvider>
    </FirestoreRefsProvider>
  );
};
EventMeeting.propTypes = {
  eventsRef: PropTypes.objectOf(PropTypes.any).isRequired,
  gcalEventId: PropTypes.string.isRequired,
  eventRef: PropTypes.objectOf(PropTypes.any).isRequired,
  guestsRef: PropTypes.objectOf(PropTypes.any).isRequired,
  gcalGuests: PropTypes.array.isRequired,
  title: PropTypes.string.isRequired,
  titleInputValue: PropTypes.string.isRequired,
  htmlLink: PropTypes.string.isRequired,
  recurring: PropTypes.bool.isRequired,
  guests: PropTypes.array.isRequired,
};

export default EventMeeting;
