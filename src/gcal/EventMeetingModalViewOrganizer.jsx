import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import { AuthContext } from './AuthProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { GcalGuestProvider } from './GcalGuestProvider.jsx';
import MyTasks from './MyTasks.jsx';
import Guests from './Guests.jsx';
import Summary from './Summary.jsx';

const EventMeetingModalViewOrganizer = ({
  guests,
}) => {
  const { gcalGuests } = useContext(GcalGuestsContext);
  const { authUserEmail } = useContext(AuthContext);

  const authGcalGuest = gcalGuests.find((g) => g.email === authUserEmail);
  // authGuest may be undefined if it doesn't exist yet in Firestore
  const authGuest = guests.find((g) => g.email === authUserEmail);

  return (
    <>
      <Box marginBottom="15px">
        <Summary guests={guests} />
      </Box>
      <Box marginBottom="15px">
        <Guests guests={guests} />
      </Box>
      <GcalGuestProvider gcalGuest={authGcalGuest}>
        <MyTasks
          tasks={(authGuest && authGuest.tasks) || {}}
          blockOffStarted={!!authGuest && !!authGuest.blockOffStarted}
          prep={(authGuest && authGuest.prep) || {}}
        />
      </GcalGuestProvider>
    </>
  );
};

EventMeetingModalViewOrganizer.propTypes = {
  guests: PropTypes.array.isRequired,
};

export default EventMeetingModalViewOrganizer;
