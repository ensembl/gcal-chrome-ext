import React from 'react';

import { Box } from '@mui/material';
import PropTypes from 'prop-types';

import MyMeetingsEvent from './MyMeetingsEvent.jsx';

const MyMeetingsDayEvents = ({
  startDateGroupDisplay,
  events,
}) => (
  <>
    <Box
      style={{ fontSize: '1rem', fontWeight: 'bold' }}
    >
      {startDateGroupDisplay}
    </Box>
    {events.map((event) => (
      <Box
        marginTop="5px"
        key={event.id || ''}
      >
        <MyMeetingsEvent
          eventId={event.id || ''}
          startDateDisplay={event.startDateDisplay || ''}
          startTimeDisplay={event.startTimeDisplay || ''}
          endDateDisplay={event.endDateDisplay || ''}
          endTimeDisplay={event.endTimeDisplay || ''}
          title={event.title || ''}
          htmlLink={event.htmlLink || ''}
          attendees={event.attendees || []}
        />
      </Box>
    ))}
  </>
);

MyMeetingsDayEvents.propTypes = {
  startDateGroupDisplay: PropTypes.string.isRequired,
  events: PropTypes.array.isRequired,
};

export default MyMeetingsDayEvents;
