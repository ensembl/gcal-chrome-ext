import React, {
  useState, useEffect, useContext,
} from 'react';

import PropTypes from 'prop-types';
import { Stack, TextField, IconButton } from '@mui/material';
import ClearIcon from '@mui/icons-material/Clear';

import { AuthContext } from './AuthProvider.jsx';
import { FirestoreRefsContext } from './FirestoreRefsProvider.jsx';
import { GcalGuestContext } from './GcalGuestProvider.jsx';
import { setTaskText, setTaskState, deleteTask } from './guestFirestore';
import { taskStateOptions } from './taskStateOptions';
import './TaskAutoSave.css';

const TaskAutoSave = ({
  initialCreatedAt,
  createdAt,
  deleteCreatedAt,
  onEnterPressed,
  autoFocus,
}) => {
  const { guestsRef } = useContext(FirestoreRefsContext);
  const { authUserEmail } = useContext(AuthContext);
  const { email } = useContext(GcalGuestContext);

  const defaultTask = email === authUserEmail ? 'Finalize agenda' : 'Prepare questions or proposals';

  const [textState, setTextState] = useState(
    createdAt === initialCreatedAt
      ? defaultTask
      : '',
  );

  const deleteFromFirestore = async () => {
    await deleteTask({
      guestsRef,
      email,
      createdAt,
    });
  };

  // If TextField becomes blank, delete task from Firestore, but leave the React component alone.
  // using deleteFromFirestore

  // If user clicks the X button, delete task from Firestoe and remove component
  // using deleteFromFirestore and deleteCreatedAt

  const updateFirestore = async () => {
    if (textState) {
      await setTaskText({
        guestsRef,
        email,
        createdAt,
        text: textState,
      });
      await setTaskState({
        guestsRef,
        email,
        createdAt,
        state: taskStateOptions[0].state,
      });
    } else {
      await deleteFromFirestore();
    }
  };

  useEffect(() => {
    const timeoutId = setTimeout(updateFirestore, 500); // https://stackoverflow.com/questions/53071774/reactjs-delay-onchange-while-typing
    return () => clearTimeout(timeoutId);
  }, [textState]);

  return (
    <Stack
      className="EnsemblTaskAutoSave"
      direction="row"
      alignItems="center"
      justifyContent="space-between">
      <TextField
        variant="standard"
        inputProps={{ style: { fontSize: '0.9rem' } }}
        value={textState}
        onChange={(e) => { setTextState(e.target.value); }}
        placeholder="e.g. Review product specs"
        fullWidth
        size="small"
        onKeyPress={(e) => {
          if (e.key === 'Enter') {
            onEnterPressed();
          }
        }}
        autoFocus={autoFocus}
      />
      <IconButton
        size="small"
        title="Delete"
        onClick={() => {
          deleteCreatedAt({ createdAt });
          deleteFromFirestore();
        }}
      >
        <ClearIcon fontSize="small" />
      </IconButton>
    </Stack>
  );
};

TaskAutoSave.propTypes = {
  initialCreatedAt: PropTypes.number.isRequired,
  createdAt: PropTypes.number.isRequired,
  deleteCreatedAt: PropTypes.func.isRequired,
  onEnterPressed: PropTypes.func.isRequired,
  autoFocus: PropTypes.bool.isRequired,
};

export default TaskAutoSave;
