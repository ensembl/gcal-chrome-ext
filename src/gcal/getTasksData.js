const getTasksData = ({ tasks }) => {
  const tasksCount = Object.keys(tasks).length;
  const hasTasks = tasksCount > 0;
  const tasksLeft = Object.values(tasks).filter(
    (task) => task.state === 'pending action' || task.state === undefined,
  );
  const tasksLeftCount = tasksLeft.length;
  const checkedAll = hasTasks && tasksLeftCount === 0;

  return {
    tasksCount, hasTasks, tasksLeft, tasksLeftCount, checkedAll,
  };
};

export { getTasksData };
