import React from 'react';

import { Box, Stack } from '@mui/material';

import EnsemblTootlip from './EnsemblTooltip.jsx';

const SummaryHeader = () => (
  <Stack
    direction="row"
    justifyContent="space-around"
    alignItems="center"
  >
    <EnsemblTootlip
      placement="top"
      title="Guests who have tasks left"
      content={<Box fontSize="2rem">⏳</Box>}
    />
    <EnsemblTootlip
      placement="top"
      title="Guests who have completed all tasks (or have no tasks)"
      content={<Box fontSize="2rem">✅</Box>}
    />
  </Stack >
);

export default SummaryHeader;
