const reasonSuggestions = [
  'Run meeting', // First suggesetion is default reason for organizer
  'Facilitate and keep time',
  'Present ideas and lead discussion',
  'Report results',
  'Give feedback and ask questions', // Last suggestion is default reason for non-organizer
];

export { reasonSuggestions };
