import { getGuestNodes, getGuestNodeDisplaySpan, getGuestNodeAvatarDiv } from './htmlNodes';

const { runtime, storage } = chrome; /* global chrome */

const getGcalGuests = () => {
  // Assume only the guests on the calendar pages have [data-hovercard-id] attributes.
  // When adding a guest, the autocomplete entries have [data-hovercard-id] attributes,
  // but no [role] attributes.
  // So also inlclude [role] attribute in the selector to avoid the autocomplete nodes.
  const gcalGuests = [];
  const { guestNodes } = getGuestNodes();
  guestNodes.forEach((guestNode) => {
    const email = guestNode.getAttribute('data-email');
    if (gcalGuests.map((guest) => guest.email).includes(email)) { return; }

    const { guestNodeDisplaySpan } = getGuestNodeDisplaySpan({ guestNode });
    const display = guestNodeDisplaySpan
      ? guestNodeDisplaySpan.childNodes[0].innerText
      : email;

    const defaultAvatarUrl = runtime.getURL('/images/default-avatar.png');
    const { guestNodeAvatarDiv } = getGuestNodeAvatarDiv({ guestNode });
    const avatarUrl = guestNodeAvatarDiv
      ? guestNodeAvatarDiv.style.backgroundImage.slice(4, -1).replace(/['"]/g, '')
      : defaultAvatarUrl;

    const ariaLabel = guestNode.getAttribute('aria-label');
    let responseStatus = '';
    if (ariaLabel.includes('Attending')) {
      responseStatus = 'accepted';
    } else if (ariaLabel.includes('Declined')) {
      responseStatus = 'declined';
    } else if (ariaLabel.includes('Maybe attending')) {
      responseStatus = 'tentative';
    }

    gcalGuests.push({
      email,
      organizer: ariaLabel.includes('Organizer'),
      responseStatus,
      display,
      avatarUrl,
    });

    storage.local.set({
      [`meetings.ensembl.so/guests/${email}`]: {
        display,
        avatarUrl,
      },
    });
  });

  return { gcalGuests };
};

export { getGcalGuests };
