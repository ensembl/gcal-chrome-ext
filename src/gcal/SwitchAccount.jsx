import React, { useContext } from 'react';

import { Alert, AlertTitle, Box } from '@mui/material';
import PropTypes from 'prop-types';

import { AuthContext } from './AuthProvider.jsx';

const SwitchAccount = ({ userHtmlEmail }) => {
  const { authUserEmail } = useContext(AuthContext);

  return (
    <Alert
      icon={false}
      severity="info"
      style={{ border: '1px solid grey' }}
    >
      <AlertTitle>Sign in with same Google account</AlertTitle>
      <Box marginBottom="10px">
        👈 You&apos;re signed into Google Calendar with <b>{userHtmlEmail}</b>,
        but signed into Ensembl with <b>{authUserEmail}</b> ☝️.
      </Box>
      <Box>
        Sign out of Ensembl and sign in again with <b>{userHtmlEmail}</b>.
      </Box>
    </Alert>

  );
};

SwitchAccount.propTypes = {
  userHtmlEmail: PropTypes.string.isRequired,
};

export default SwitchAccount;
