import React from 'react';

import TaskStateIcon from './TaskStateIcon.jsx';

const taskStateOptions = [
  {
    state: 'pending action',
    icon: <TaskStateIcon state="pending action" />,
    display: 'Pending action',
  },
  {
    state: 'done',
    icon: <TaskStateIcon state="done" />,
    display: 'Task done',
  },
  {
    state: 'not understand',
    icon: <TaskStateIcon state="not understand" />,
    display: 'I don\'t understand this task',
  },
  {
    state: 'discuss meeting',
    icon: <TaskStateIcon state="discuss meeting" />,
    display: 'Prefer to discuss in meeting',
  },
];

export { taskStateOptions };
