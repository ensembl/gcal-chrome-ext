import React, { useState, useContext } from 'react';

import PropTypes from 'prop-types';
import pluralize from 'pluralize';
import { Avatar, Box, Button } from '@mui/material';

import { reminderSentTooltip } from './reminderSentTooltip';
import { getTasksData } from './getTasksData';
import { remainTasksReminder } from './guestCallables';
import { GcalGuestContext } from './GcalGuestProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { AuthContext } from './AuthProvider.jsx';
import { GcalEventContext } from './GcalEventProvider.jsx';
import EnsemblTootlip from './EnsemblTooltip.jsx';

const SummaryGuest = ({ tasks }) => {
  const { authUserName, authUserEmail } = useContext(AuthContext);
  const { email, display, avatarUrl } = useContext(GcalGuestContext);
  const { orgDisplay } = useContext(GcalGuestsContext);
  const { title, htmlLink } = useContext(GcalEventContext);

  const {
    tasksCount, hasTasks, tasksLeft, tasksLeftCount, checkedAll,
  } = getTasksData({ tasks });

  const [reminderSent, setReminderSent] = useState(false);

  const getTooltipText = () => {
    if (!hasTasks) {
      return `${display} has no tasks`;
    }
    if (checkedAll) {
      return `${display} completed all ${tasksCount} ${pluralize('task', tasksCount)}`;
    }
    return `${display} has ${tasksLeftCount} ${pluralize('task', tasksLeftCount)} left`;
  };

  const onClick = async () => {
    setReminderSent(true);
    try {
      await remainTasksReminder({
        title,
        htmlLink,
        orgDisplay,
        notifyEmail: email,
        authUserName,
        authUserEmail,
        remainingTasksString: JSON.stringify(tasksLeft),
      });
    } catch (error) {
      console.error('Error calling remainTasksReminder', error);
    }
  };

  const customTitle = (
    <>
      <Box>{getTooltipText()}</Box>
      {tasksLeftCount > 0 && (
        <Box marginTop="10px" style={{ textAlign: 'center' }}>
          {reminderSent && reminderSentTooltip}
          {!reminderSent && (
            <Button
              variant="outlined"
              size="small"
              onClick={onClick}
              disableElevation
            >
              {`Remind ${display}`}
            </Button>
          )}
        </Box>
      )}
    </>
  );

  return (
    <Box
      display="flex"
      justifyContent="center"
      alignItems="center"
    >
      <EnsemblTootlip
        placement="top"
        title={customTitle}
        content={
          <Avatar
            style={{ height: '28px', width: '28px' }}
            src={avatarUrl}
          />
        }
      />
    </Box>
  );
};

SummaryGuest.propTypes = {
  tasks: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default SummaryGuest;
