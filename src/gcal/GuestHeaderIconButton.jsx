import React from 'react';

import PropTypes from 'prop-types';
import { IconButton } from '@mui/material';

import EnsemblTootlip from './EnsemblTooltip.jsx';

const GuestHeaderIconButton = ({
  getTooltip,
  disabled,
  onClick,
  icon,
}) => (
  <EnsemblTootlip
    placement="top"
    title={getTooltip()}
    content={
      <div> {/* https://github.com/mui-org/material-ui/issues/8416 */}
        <IconButton
          color="primary"
          size="small"
          disabled={disabled}
          onClick={onClick}
          sx={{
            '&:hover': {
              backgroundColor: 'white',
            },
          }}
        >
          {icon}
        </IconButton>
      </div>
    }
  />
);

GuestHeaderIconButton.propTypes = {
  getTooltip: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
  onClick: PropTypes.func.isRequired,
  icon: PropTypes.node.isRequired,
};

export default GuestHeaderIconButton;
