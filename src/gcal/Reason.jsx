import React, { useState, useEffect, useContext } from 'react';

import { Box } from '@mui/material';
import PropTypes from 'prop-types';

import { reasonSuggestions } from './reasonSuggestions';
import { ViewModeContext } from './ViewModeProvider.jsx';
import { AuthContext } from './AuthProvider.jsx';
import { FirestoreRefsContext } from './FirestoreRefsProvider.jsx';
import { GcalGuestContext } from './GcalGuestProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { GcalEventContext } from './GcalEventProvider.jsx';
import { setReason } from './guestFirestore';
import { reasonSetNotify } from './guestCallables';
import { getNotifyEmails } from './getNotifyEmails';
import ReasonView from './ReasonView.jsx';
import ReasonEdit from './ReasonEdit.jsx';
import ReasonAddButton from './ReasonAddButton.jsx';

const Reason = ({
  reason,
  inFirestore,
}) => {
  const { authUserName, authUserEmail } = useContext(AuthContext);
  const { autoSave } = useContext(ViewModeContext);
  const { guestsRef } = useContext(FirestoreRefsContext);
  const { email, display } = useContext(GcalGuestContext);
  const { title, htmlLink } = useContext(GcalEventContext);
  const { orgEmail } = useContext(GcalGuestsContext);

  // Default reason applies only in autoSave mode (by design) since reasonState
  // is overwritten with blank reason when entering manual edit mode of a blank reason
  const defaultReason = email === orgEmail
    ? reasonSuggestions[0]
    : reasonSuggestions[reasonSuggestions.length - 1];
  const [reasonState, setReasonState] = useState(inFirestore ? reason : defaultReason);
  const [editMode, setEditMode] = useState(false);
  const [waitingFirestore, setWaitingFirestore] = useState(false);

  const reasonDisplay = waitingFirestore ? reasonState : reason;

  const exitEditMode = () => { setEditMode(false); };

  const enterEditMode = () => {
    setReasonState(reason);
    setEditMode(true);
  };

  const setToFirestore = async () => {
    setWaitingFirestore(true);
    await setReason({
      guestsRef,
      email,
      reason: reasonState,
    });
  };

  const setToFirestoreAndNotify = async () => {
    const beforeReason = reason;
    await setToFirestore();
    if (!autoSave) {
      const { notifyEmails } = getNotifyEmails({
        authUserEmail, email, orgEmail,
      });
      if (notifyEmails.length > 0) {
        await reasonSetNotify({
          title,
          htmlLink,
          authUserName,
          authUserEmail,
          display,
          notifyEmails,
          beforeReason,
          reason: reasonState,
        });
      }
    }
  };

  useEffect(() => {
    if (autoSave) {
      const timeoutId = setTimeout(setToFirestore, 500); // https://stackoverflow.com/questions/53071774/reactjs-delay-onchange-while-typing
      return () => clearTimeout(timeoutId);
    }
    return 0;
  }, [reasonState]);

  useEffect(() => {
    setWaitingFirestore(false);
  }, [reason]);

  return (
    <>
      <Box fontWeight="500">
        Reason
      </Box>
      {!autoSave && !editMode && !reasonDisplay && (
        <Box>
          <ReasonAddButton
            enterEditMode={enterEditMode}
          />
        </Box>
      )}
      {!autoSave && !editMode && reasonDisplay && (
        <ReasonView
          reason={reasonDisplay}
          enterEditMode={enterEditMode}
        />
      )}
      {(autoSave || editMode) && (
        <ReasonEdit
          reasonState={reasonState}
          setReasonState={setReasonState}
          reason={reason}
          setToFirestoreAndNotify={setToFirestoreAndNotify}
          exitEditMode={exitEditMode}
        />
      )}
    </>
  );
};

Reason.propTypes = {
  reason: PropTypes.string.isRequired,
  inFirestore: PropTypes.bool.isRequired,
};

export default Reason;
