import React from 'react';

import {
  Card, Box, Stack, TextField,
} from '@mui/material';
import PropTypes from 'prop-types';

import EditButtons from './EditButtons.jsx';

const TaskEdit = ({
  textState,
  setTextState,
  text,
  setTextToFirestoreAndNotify,
  deleteFromFirestoreAndNotify,
  exitEditMode,
}) => (
  <Card
    variant="outlined"
    style={{ padding: '10px 7px' }}
  >
    <Stack direction="row" alignItems="center" justifyContent="space-between">
      <TextField
        variant="standard"
        inputProps={{ style: { fontSize: 14 } }}
        value={textState}
        onChange={(e) => { setTextState(e.target.value); }}
        placeholder="e.g. Review product specs"
        fullWidth
        size="small"
        autoFocus
      />
    </Stack>
    <Box marginTop="10px" marginLeft="3px">
      <EditButtons
        saveDisabled={textState === text || textState === ''}
        clickSave={setTextToFirestoreAndNotify}
        clickDelete={deleteFromFirestoreAndNotify}
        exitEditMode={exitEditMode}
      />
    </Box>
  </Card >
);

TaskEdit.propTypes = {
  textState: PropTypes.string.isRequired,
  setTextState: PropTypes.func.isRequired,
  text: PropTypes.string.isRequired,
  setTextToFirestoreAndNotify: PropTypes.func.isRequired,
  deleteFromFirestoreAndNotify: PropTypes.func,
  exitEditMode: PropTypes.func.isRequired,
};

TaskEdit.defaultProps = {
  deleteFromFirestoreAndNotify: null,
};

export default TaskEdit;
