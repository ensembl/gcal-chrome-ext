import React from 'react';

import PropTypes from 'prop-types';
import { Stack, Avatar } from '@mui/material';

const LikeAvatars = ({
  likedGcalGuests,
}) => (
  <Stack
    direction="row"
    alignItems="center"
    spacing="5px"
  >
    {likedGcalGuests.map((g) => (
      <Avatar
        key={g.email}
        style={{ height: '24px', width: '24px' }}
        src={g.avatarUrl}
      />
    ))}
  </Stack>
);

LikeAvatars.propTypes = {
  likedGcalGuests: PropTypes.array.isRequired,
};

export default LikeAvatars;
