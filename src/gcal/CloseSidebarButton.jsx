import React from 'react';

import { IconButton } from '@mui/material';
import DoubleArrowIcon from '@mui/icons-material/DoubleArrow';
import { setSidebarShow, setContainerVisibility } from './sidebar';

const CloseSidebarButton = () => {
  const onClick = () => {
    setSidebarShow({ show: false });
    setContainerVisibility({ visibility: 'hidden' });
  };
  return (
    <IconButton
      title="Close Ensembl sidebar"
      size="small"
      variant="outlined"
      color="primary"
      onClick={onClick}
    >
      <DoubleArrowIcon fontSize="small" />
    </IconButton >
  );
};

export default CloseSidebarButton;
