import React from 'react';

import { Badge, Box } from '@mui/material';
import CheckBoxOutlineBlank from '@mui/icons-material/CheckBoxOutlineBlank';
import CheckIcon from '@mui/icons-material/Check';
import CheckCircleOutlineIcon from '@mui/icons-material/CheckCircleOutline';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';
import PeopleOutlineIcon from '@mui/icons-material/PeopleOutline';
import PropTypes from 'prop-types';

import './TaskStateIcon.css';

const TaskStateIcon = ({
  state,
}) => {
  let icon;

  if (state === 'pending action') {
    icon = (
      <Badge
        overlap="rectangular"
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        badgeContent={0}
      >
        <CheckBoxOutlineBlank fontSize="small" style={{ color: 'black' }} />
      </Badge>
    );
  }

  if (state === 'done') {
    icon = (
      <Badge
        overlap="rectangular"
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        badgeContent={<CheckIcon style={{ color: 'rgb(76, 175, 80)', fontSize: '15px' }} />}
      >
        <CheckCircleOutlineIcon fontSize="small" style={{ color: 'black' }} />
      </Badge>
    );
  }

  if (state === 'not understand') {
    icon = (
      <Badge
        overlap="rectangular"
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        badgeContent={<CheckIcon style={{ color: 'rgb(76, 175, 80)', fontSize: '15px' }} />}
      >
        <HelpOutlineIcon fontSize="small" style={{ color: 'black' }} />
      </Badge>
    );
  }

  if (state === 'discuss meeting') {
    icon = (
      <Badge
        overlap="rectangular"
        anchorOrigin={{ vertical: 'bottom', horizontal: 'right' }}
        badgeContent={<CheckIcon style={{ color: 'rgb(76, 175, 80)', fontSize: '15px' }} />}
      >
        <PeopleOutlineIcon fontSize="small" style={{ color: 'black' }} />
      </Badge>
    );
  }

  return (
    <Box className="EnsemblTaskStateIcon">
      {icon}
    </Box>
  );
};

TaskStateIcon.propTypes = {
  state: PropTypes.string.isRequired,
};

export default TaskStateIcon;
