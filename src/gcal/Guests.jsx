import React, { useContext, useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import GuestsHeader from './GuestsHeader.jsx';
import GuestsBody from './GuestsBody.jsx';
import { AuthContext } from './AuthProvider.jsx';
import { ViewModeContext } from './ViewModeProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';

const Guests = ({
  guests,
  setGuestsChanged,
}) => {
  const { authUserEmail } = useContext(AuthContext);
  const { modalViewEvent } = useContext(ViewModeContext);
  const { gcalGuests } = useContext(GcalGuestsContext);

  const [showBodies, setShowBodies] = useState({});

  const getNewShowBodies = ({ show }) => {
    const newShowBodies = {};
    let gcalGuestsShow = gcalGuests;
    if (modalViewEvent) {
      gcalGuestsShow = gcalGuests.filter((g) => g.email !== authUserEmail);
    }
    gcalGuestsShow.forEach((g) => { newShowBodies[g.email] = show; });
    return { newShowBodies };
  };

  const toggleShowBodies = () => {
    const allShowing = Object.entries(showBodies).every(([, show]) => show);
    const { newShowBodies } = getNewShowBodies({ show: !allShowing });
    setShowBodies(newShowBodies);
  };

  useEffect(() => {
    const { newShowBodies } = getNewShowBodies({ show: false });
    setShowBodies(newShowBodies);
  }, [gcalGuests]);

  return (
    <>
      <GuestsHeader
        showBodies={showBodies}
        toggleShowBodies={toggleShowBodies}
      />
      <Box marginTop="5px">
        <GuestsBody
          guests={guests}
          setGuestsChanged={setGuestsChanged}
          showBodies={showBodies}
          setShowBodies={setShowBodies}
        />
      </Box>
    </>
  );
};

Guests.propTypes = {
  guests: PropTypes.array.isRequired,
  setGuestsChanged: PropTypes.func,
};

Guests.defaultProps = {
  setGuestsChanged: null,
};

export default Guests;
