import { eventCreatedNotify } from './eventCallables';

const { runtime } = chrome; /* global chrome */

const notifyNewEvent = ({ newEventId }) => {
  runtime.sendMessage(
    {
      msg: 'get-chrome-token',
      payload: { eventId: newEventId },
    },
    (response) => {
      const { token, payload } = response;
      const { eventId } = payload;
      eventCreatedNotify({ gcalEventId: eventId, token });
    },
  );
};

export { notifyNewEvent };
