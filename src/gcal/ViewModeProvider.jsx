import React from 'react';

import PropTypes from 'prop-types';

export const ViewModeContext = React.createContext();

export const ViewModeProvider = ({
  autoSave,
  modalViewEvent,
  modalNewEvent,
  children,
}) => (
  <ViewModeContext.Provider
    value={{
      autoSave,
      modalViewEvent,
      modalNewEvent,
    }}
  >
    {children}
  </ViewModeContext.Provider>
);

ViewModeProvider.propTypes = {
  autoSave: PropTypes.bool.isRequired,
  modalViewEvent: PropTypes.bool.isRequired,
  modalNewEvent: PropTypes.bool.isRequired,
  children: PropTypes.node.isRequired,
};

export default ViewModeProvider;
