import React, { useEffect, useState } from 'react';

import { Card, Stack, Box } from '@mui/material';
import PropTypes from 'prop-types';

import MyMeetingsEventDateTime from './MyMeetingsEventDateTime.jsx';
import MyMeetingsEventTitle from './MyMeetingsEventTitle.jsx';
import MyMeetingsEventAttendee from './MyMeetingsEventAttendee.jsx';

const MyMeetingsEvent = ({
  eventId,
  startDateDisplay,
  startTimeDisplay,
  endDateDisplay,
  endTimeDisplay,
  title,
  htmlLink,
  attendees,
}) => {
  const [sortedAttendees, setSortedAttendees] = useState([]);

  const compareAttendees = (a, b) => {
    if (a.responseStatus === 'accepted') {
      return -1;
    }
    if (a.responseStatus === 'declined' && b.responseStatus !== 'accepted') {
      return -1;
    }
    if (a.responseStatus === 'tentative' && b.responseStatus !== 'accepted' && b.responseStatus !== 'declined') {
      return -1;
    }
    return 1;
  };

  useEffect(() => {
    setSortedAttendees(attendees.sort(compareAttendees));
  }, []);

  return (
    <Card
      variant="outlined"
      style={{ padding: 10 }}
    >
      <MyMeetingsEventDateTime
        startDateDisplay={startDateDisplay}
        startTimeDisplay={startTimeDisplay}
        endDateDisplay={endDateDisplay}
        endTimeDisplay={endTimeDisplay}
      />
      <Box marginTop="3px">
        <MyMeetingsEventTitle
          eventId={eventId}
          title={title}
          htmlLink={htmlLink}
        />
      </Box>
      {attendees.length > 0 && (
        <Stack direction="row" alignItems="center" flexWrap="wrap" gap="4px" marginTop="3px">
          {sortedAttendees.map((attendee) => (
            <MyMeetingsEventAttendee
              key={attendee.email || ''}
              email={attendee.email || ''}
              responseStatus={attendee.responseStatus || ''}
            />
          ))}
        </Stack>
      )}
    </Card >
  );
};

MyMeetingsEvent.propTypes = {
  eventId: PropTypes.string.isRequired,
  startDateDisplay: PropTypes.string.isRequired,
  startTimeDisplay: PropTypes.string.isRequired,
  endDateDisplay: PropTypes.string.isRequired,
  endTimeDisplay: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  htmlLink: PropTypes.string.isRequired,
  attendees: PropTypes.array.isRequired,
};

export default MyMeetingsEvent;
