import React from 'react';
import { Box, Card } from '@mui/material';

import PropTypes from 'prop-types';

import OrganizerYourReason from './OrganizerYourReason.jsx';
import BlockOff from './BlockOff.jsx';
import Tasks from './Tasks.jsx';
import TasksLeft from './TasksLeft.jsx';

const Attendee = ({
  reason,
  tasks,
  blockOffStarted,
  prep,
}) => {
  const hasTasks = Object.keys(tasks).length > 0;
  const tasksLeft = hasTasks ? Object.values(tasks).filter((task) => task.state === 'pending action' || task.state === undefined).length : 0;

  const narrow = window.matchMedia('(max-width: 1300px)').matches;

  return (
    <Card
      variant="outlined"
      style={{ padding: '10px 15px' }}
    >
      <OrganizerYourReason
        reason={reason}
      />
      <TasksLeft
        hasTasks={hasTasks}
        tasksLeft={tasksLeft}
        narrow={narrow}
      />
      <Tasks
        tasks={tasks}
        noHeader
      />
      <Box marginTop="10px">
        <BlockOff
          auto={false}
          blockOffStarted={blockOffStarted}
          prep={prep}
        />
      </Box>
    </Card>
  );
};

Attendee.propTypes = {
  reason: PropTypes.string.isRequired,
  tasks: PropTypes.objectOf(PropTypes.any).isRequired,
  blockOffStarted: PropTypes.bool.isRequired,
  prep: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default Attendee;
