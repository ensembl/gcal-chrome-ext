import React from 'react';

import PropTypes from 'prop-types';
import pluralize from 'pluralize';
import { Box } from '@mui/material';

const TasksLeft = ({
  hasTasks,
  tasksLeft,
  narrow,
}) => (
  <Box
    fontWeight="500"
    fontSize={narrow ? '1.2rem' : '1.8rem'}
    paddingY="20px"
    paddingX="10px"
    textAlign="center"
  >
    {hasTasks && tasksLeft > 0 && `${tasksLeft} ${pluralize('task', tasksLeft)} left`}
    {hasTasks && tasksLeft === 0 && 'All set 😊'}
    {!hasTasks && 'All set 😊'}
  </Box>
);

TasksLeft.propTypes = {
  hasTasks: PropTypes.bool.isRequired,
  tasksLeft: PropTypes.number.isRequired,
  narrow: PropTypes.bool.isRequired,
};

export default TasksLeft;
