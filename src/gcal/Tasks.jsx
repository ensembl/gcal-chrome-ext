import React, { useState } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import Task from './Task.jsx';
import TaskAdd from './TaskAdd.jsx';
import { getTasksData } from './getTasksData';

const Tasks = ({
  tasks,
  noHeader,
}) => {
  const [addTaskEditMode, setAddTaskEditMode] = useState(false);
  const { tasksLeftCount } = getTasksData({ tasks });

  return (
    <>
      {!noHeader && (
        <Box fontWeight="500">
          Tasks
        </Box>
      )}
      {Object.entries(tasks)
        .sort((a, b) => parseInt(a[0], 10) - parseInt(b[0], 10))
        .map(([createdAt, task]) => (
          <Task
            key={createdAt}
            createdAt={createdAt}
            text={task.text}
            state={task.state || 'pending action'}
            tasksLeftCount={tasksLeftCount}
          />
        ))}
      <TaskAdd
        editMode={addTaskEditMode}
        setEditMode={setAddTaskEditMode}
      />
    </>
  );
};

Tasks.propTypes = {
  tasks: PropTypes.objectOf(PropTypes.any).isRequired,
  noHeader: PropTypes.bool.isRequired,
};

Tasks.defaultProps = {
  noHeader: false,
};

export default Tasks;
