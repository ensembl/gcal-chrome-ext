import React, { useState, useContext, useEffect } from 'react';

import PropTypes from 'prop-types';
import { Button } from '@mui/material';

import { AuthContext } from './AuthProvider.jsx';
import { FirestoreRefsContext } from './FirestoreRefsProvider.jsx';
import { GcalGuestContext } from './GcalGuestProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { GcalEventContext } from './GcalEventProvider.jsx';
import { setTaskText, setTaskState } from './guestFirestore';
import { taskSetTextNotify } from './guestCallables';
import { taskStateOptions } from './taskStateOptions';
import TaskEdit from './TaskEdit.jsx';

const TaskAdd = ({
  editMode,
  setEditMode,
}) => {
  const { authUserName, authUserEmail } = useContext(AuthContext);
  const { guestsRef } = useContext(FirestoreRefsContext);
  const { email, display, avatarUrl } = useContext(GcalGuestContext);
  const { title, htmlLink } = useContext(GcalEventContext);
  const { orgEmail, orgDisplay, orgAvatarUrl } = useContext(GcalGuestsContext);
  const [notifyGuests, setNotifyGuests] = useState([]);

  const [textState, setTextState] = useState('');

  const enterEditMode = () => {
    setTextState('');
    setEditMode(true);
  };

  const exitEditMode = () => { setEditMode(false); };

  const addTaskToFirestoreAndNotify = async () => {
    const createdAt = `${new Date().getTime()}`;
    await setTaskText({
      guestsRef,
      email,
      createdAt,
      text: textState,
    });
    await setTaskState({
      guestsRef,
      email,
      createdAt,
      state: taskStateOptions[0].state,
    });
    if (notifyGuests.length > 0) {
      await taskSetTextNotify({
        title,
        htmlLink,
        authUserName,
        authUserEmail,
        display,
        notifyEmails: notifyGuests.map((notifyGuest) => notifyGuest.email),
        beforeText: '',
        text: textState,
      });
    }
  };

  useEffect(() => {
    const newNotifyGuests = [];
    if (authUserEmail !== email) {
      newNotifyGuests.push({ email, display, avatarUrl });
    }
    if (authUserEmail !== orgEmail && orgEmail !== email) {
      newNotifyGuests.push({ email: orgEmail, display: orgDisplay, avatarUrl: orgAvatarUrl });
    }
    setNotifyGuests(newNotifyGuests);
  }, []);

  return (
    <>
      {editMode && (
        <TaskEdit
          textState={textState}
          setTextState={setTextState}
          setTextToFirestoreAndNotify={addTaskToFirestoreAndNotify}
          exitEditMode={exitEditMode}
        />
      )}
      {!editMode && (
        <Button
          style={{ paddingLeft: 0, paddingRight: 0 }}
          onClick={enterEditMode}
          color='primary'
        >
          Add task
        </Button>
      )}
    </>
  );
};

TaskAdd.propTypes = {
  editMode: PropTypes.bool.isRequired,
  setEditMode: PropTypes.func.isRequired,
};

export default TaskAdd;
