import React from 'react';

import PropTypes from 'prop-types';
import ThumbUpAltIcon from '@mui/icons-material/ThumbUpAlt';
import ThumbUpOffAltIcon from '@mui/icons-material/ThumbUpOffAlt';
import { Box, CircularProgress, IconButton } from '@mui/material';

const LikeButton = ({
  progress,
  liked,
  setLikedToFirestore,
}) => (
  <Box sx={{ position: 'relative', display: 'inline-flex' }}>
    <CircularProgress
      variant="determinate"
      thickness={3}
      size="2.3rem"
      value={100}
      style={{
        color: 'rgb(206, 234, 214)',
      }}
    />
    <CircularProgress
      variant="determinate"
      thickness={3}
      size='2.3rem'
      value={progress}
      style={{
        color: 'rgb(76, 175, 80)',
        position: 'absolute',
        left: 0,
      }}
    />
    <Box
      sx={{
        top: 0,
        left: 0,
        bottom: 0,
        right: 0,
        position: 'absolute',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <IconButton
        title={liked ? 'Unlike' : 'Like'}
        size="small"
        variant="outlined"
        onClick={setLikedToFirestore}
      >
        {liked && <ThumbUpAltIcon fontSize="small" />}
        {!liked && <ThumbUpOffAltIcon fontSize="small" />}
      </IconButton >
    </Box>
  </Box>
);

LikeButton.propTypes = {
  progress: PropTypes.number.isRequired,
  liked: PropTypes.bool.isRequired,
  setLikedToFirestore: PropTypes.func.isRequired,
};

export default LikeButton;
