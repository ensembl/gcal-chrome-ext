import React, { useState, useEffect } from 'react';

import PropTypes from 'prop-types';
import { Box, CircularProgress } from '@mui/material';

import { AuthProvider } from './AuthProvider.jsx';
import SignInSidebar from './SignInSidebar.jsx';
import AppHeader from './AppHeader.jsx';
import AppBody from './AppBody.jsx';

import './App.css';

const App = ({
  gcalEventId,
  flatGcalGuests,
  autoSave,
  modalViewEvent,
  modalNewEvent,
  calendarView,
  titleInputValue,
  userHtmlEmail,
  pageViewRecurring,
}) => {
  const [authUser, setAuthUser] = useState(null);
  const [authLoaded, setAuthLoaded] = useState(false);

  const [eventReady, setEventReady] = useState(false);
  const [myMeetingsReady, setMyMeetingsReady] = useState(false);
  const [sidebarReady, setSidebarReady] = useState(false);

  useEffect(() => {
    setSidebarReady(
      authLoaded && (
        !authUser
        || (authUser && !calendarView && eventReady)
        || (authUser && calendarView && myMeetingsReady)
        || (authUser.email !== userHtmlEmail)
      ),
    );
  }, [authLoaded, authUser, eventReady, myMeetingsReady]);

  return (
    <Box className="EnsemblApp">
      {!sidebarReady && <Box paddingY="15px" paddingX="20px"><CircularProgress size={30} /></Box>}

      <Box sx={{ display: sidebarReady ? 'block' : 'none' }}>
        {!authUser && (
          <SignInSidebar
            setAuthLoaded={setAuthLoaded}
            setAuthUser={setAuthUser}
          />
        )}
        {authUser && (
          <>
            <AppHeader
              displayName={authUser.displayName || ''}
              email={authUser.email || ''}
              avatarUrl={authUser.photoURL || ''}
              setEventReady={setEventReady}
              setMyMeetingsReady={setMyMeetingsReady}
            />
            <AuthProvider
              authUser={authUser}
            >
              <AppBody
                gcalEventId={gcalEventId}
                flatGcalGuests={flatGcalGuests}
                autoSave={autoSave}
                modalViewEvent={modalViewEvent}
                modalNewEvent={modalNewEvent}
                calendarView={calendarView}
                titleInputValue={titleInputValue}
                userHtmlEmail={userHtmlEmail}
                pageViewRecurring={pageViewRecurring}
                setEventReady={setEventReady}
                setMyMeetingsReady={setMyMeetingsReady}
              />
            </AuthProvider>
          </>
        )}
      </Box>
    </Box>
  );
};

App.propTypes = {
  authUserEmail: PropTypes.string.isRequired,
  gcalEventId: PropTypes.string.isRequired,
  flatGcalGuests: PropTypes.string.isRequired,
  autoSave: PropTypes.bool.isRequired,
  modalViewEvent: PropTypes.bool.isRequired,
  modalNewEvent: PropTypes.bool.isRequired,
  calendarView: PropTypes.bool.isRequired,
  titleInputValue: PropTypes.string.isRequired,
  userHtmlEmail: PropTypes.string.isRequired,
  pageViewRecurring: PropTypes.bool.isRequired,
};

export default App;
