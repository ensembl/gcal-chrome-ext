import React from 'react';

import PropTypes from 'prop-types';

export const FirestoreRefsContext = React.createContext();

export const FirestoreRefsProvider = ({
  eventsRef, eventRef, guestsRef, children,
}) => (
  <FirestoreRefsContext.Provider
    value={{
      eventsRef, eventRef, guestsRef,
    }}
  >
    {children}
  </FirestoreRefsContext.Provider>
);

FirestoreRefsProvider.propTypes = {
  eventsRef: PropTypes.objectOf(PropTypes.any).isRequired,
  eventRef: PropTypes.objectOf(PropTypes.any).isRequired,
  guestsRef: PropTypes.objectOf(PropTypes.any).isRequired,
  children: PropTypes.node.isRequired,
};

export default FirestoreRefsProvider;
