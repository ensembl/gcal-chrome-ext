import React from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import BlockOffAutoBody from './BlockOffAutoBody.jsx';
import BlockOffManualBody from './BlockOffManualBody.jsx';

const BlockOff = ({
  auto,
  blockOffStarted,
  prep,
}) => (
  <>
    <Box marginBottom="5px" fontWeight="500">
      Block off time
    </Box>
    {auto && <BlockOffAutoBody />}
    {!auto && (
      <BlockOffManualBody
        blockOffStarted={blockOffStarted}
        prep={prep}
      />
    )}
  </>
);

BlockOff.propTypes = {
  auto: PropTypes.bool.isRequired,
  blockOffStarted: PropTypes.bool.isRequired,
  prep: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default BlockOff;
