import { setGcalEventInfoFirestore } from './eventFirestore';

const { runtime } = chrome; /* global chrome */

const setGcalEventInfo = ({
  gcalEventId,
  setTitle,
  setHtmlLink,
  setRecurringEventId = null,
  setGcalEventLoading = null,
  eventRef,
}) => {
  runtime.sendMessage({
    msg: 'get-gcal-event',
    payload: { eventId: gcalEventId },
  }, (response) => {
    const {
      htmlLink, summary, recurringEventId = '', start,
    } = response;
    const title = summary || '(No title)';
    setTitle(title);
    setHtmlLink(htmlLink);
    if (setRecurringEventId) {
      setRecurringEventId(recurringEventId);
    }
    if (setGcalEventLoading) {
      setGcalEventLoading(false);
    }
    setGcalEventInfoFirestore({
      eventRef,
      title,
      startTime: start && start.dateTime ? new Date(start.dateTime) : '',
      updatedAt: new Date(),
    });
  });
};

export { setGcalEventInfo };
