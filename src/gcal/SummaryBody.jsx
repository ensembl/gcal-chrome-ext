import React from 'react';

import PropTypes from 'prop-types';
import { Stack } from '@mui/material';

import SummaryGuests from './SummaryGuests.jsx';

const SummaryBody = ({ guests }) => (
  <Stack
    direction="row"
    justifyContent="space-around"
  >
    <SummaryGuests
      guests={guests}
    />
    <SummaryGuests
      guests={guests}
      finished
    />
  </Stack>
);

SummaryBody.propTypes = {
  guests: PropTypes.array.isRequired,
};

export default SummaryBody;
