import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Button } from '@mui/material';

import { ViewModeContext } from './ViewModeProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { getPageNewSaveButtonDiv } from './htmlNodes';

const SaveMeetingButton = ({ disabled }) => {
  const { autoSave } = useContext(ViewModeContext);
  const { gcalGuests } = useContext(GcalGuestsContext);

  const onClick = () => {
    const { saveButtonDiv } = getPageNewSaveButtonDiv();
    if (saveButtonDiv) {
      saveButtonDiv.click();
    }
  };

  return (
    <Button
      fullWidth
      variant="contained"
      disableElevation
      onClick={onClick}
      disabled={disabled}
    >
      {autoSave && `Create meeting${gcalGuests.length > 1 ? ' and notify guests' : ''}`}
      {!autoSave && 'Save meeting'}
    </Button>
  );
};

SaveMeetingButton.propTypes = {
  disabled: PropTypes.bool.isRequired,
};

export default SaveMeetingButton;
