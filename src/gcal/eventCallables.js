import { functions } from '../firebase-config';

const eventCreatedNotify = async (data) => {
  const callable = functions.httpsCallable('eventCreatedNotify-eventCreatedNotify');
  try {
    return await callable(data);
  } catch (error) {
    throw new Error('Error calling eventCreatedNotify-eventCreatedNotify: ', error);
  }
};

export { eventCreatedNotify };
