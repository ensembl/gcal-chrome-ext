const createElement = ({
  tag,
  innerText = null,
  attributes = [],
  classNames = [],
}) => {
  const element = document.createElement(tag);
  if (innerText) {
    element.innerText = innerText;
  }
  attributes.forEach((attribute) => {
    const { key, value } = attribute;
    element.setAttribute(key, value);
  });
  classNames.forEach((className) => {
    element.classList.add(className);
  });
  return { element };
};

export {
  createElement,
};
