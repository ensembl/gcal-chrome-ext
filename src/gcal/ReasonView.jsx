import React from 'react';

import PropTypes from 'prop-types';
import Linkify from 'linkify-react';
import { Stack, Box, IconButton } from '@mui/material';
import EditIcon from '@mui/icons-material/Edit';

import './ReasonView.css';

const ReasonView = ({
  reason, enterEditMode,
}) => (
  <Stack
    className="EnsemblReasonView"
    direction="row"
    alignItems="center"
    justifyContent="space-between"
  >
    <Box
      component="span"
      className="EnsemblDontBreakOut"
    >
      <Linkify options={{ target: '_blank' }}>
        {reason}
      </Linkify>
    </Box>
    <IconButton
      size="small"
      title="Edit reason"
      onClick={enterEditMode}
    >
      <EditIcon fontSize="small" />
    </IconButton>
  </Stack>
);

ReasonView.propTypes = {
  reason: PropTypes.string.isRequired,
  enterEditMode: PropTypes.string.isRequired,
};

export default ReasonView;
