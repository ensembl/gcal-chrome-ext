import React from 'react';

import {
  Box, Stack, Button, Avatar, Popover,
} from '@mui/material';
import PropTypes from 'prop-types';
import { firebase } from '../firebase-config';

const { runtime } = chrome; /* global chrome */
const auth = firebase.auth();

const SignOut = ({
  displayName,
  email,
  avatarUrl,
  setEventReady,
  setMyMeetingsReady,
}) => {
  const [anchorEl, setAnchorEl] = React.useState(null);

  const signOut = () => {
    auth.signOut();
    runtime.sendMessage({ msg: 'sign-out-and-revoke-token' });
    setEventReady(false);
    setMyMeetingsReady(false);
  };

  const onClick = (e) => {
    setAnchorEl(e.currentTarget);
  };

  const onClose = () => {
    setAnchorEl(null);
  };

  return (
    <Stack direction="row" alignItems="center" spacing="6px">
      <Avatar
        style={{ height: '24px', width: '24px', cursor: 'pointer' }}
        src={avatarUrl}
        onClick={onClick}
      />
      <Popover
        open={!!anchorEl}
        anchorEl={anchorEl}
        onClose={onClose}
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'left',
        }}
        disablePortal
      >
        <Box paddingY="20px" paddingX="15px">
          <Box marginBottom="10px">
            <span>Signed in with</span>
            <Box component="span" style={{ fontWeight: 500 }} >{` ${email}`}</Box>
          </Box>
          <Button
            onClick={signOut}
            color="primary"
          >
            Sign out
          </Button>
        </Box>
      </Popover>
      <Box
        style={{ fontWeight: 500 }}
      >
        {displayName}
      </Box>
    </Stack>
  );
};

SignOut.propTypes = {
  displayName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  setEventReady: PropTypes.func.isRequired,
  setMyMeetingsReady: PropTypes.func.isRequired,
};

export default SignOut;
