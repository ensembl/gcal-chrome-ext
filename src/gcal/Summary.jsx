import React from 'react';

import PropTypes from 'prop-types';

import { Box, Card } from '@mui/material';

import SummaryHeader from './SummaryHeader.jsx';
import SummaryBody from './SummaryBody.jsx';

const Summary = ({ guests }) => (
  <Card
    variant="outlined"
    style={{ padding: '20px 15px' }}
  >
    <Box marginBottom="20px">
      <SummaryHeader />
    </Box>
    <SummaryBody
      guests={guests}
    />
  </Card>
);

Summary.propTypes = {
  guests: PropTypes.array.isRequired,
};

export default Summary;
