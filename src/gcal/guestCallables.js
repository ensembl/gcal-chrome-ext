import { functions } from '../firebase-config';

const reasonSetNotify = async (data) => {
  const callable = functions.httpsCallable('reasonSetNotify-reasonSetNotify');
  try {
    return await callable(data);
  } catch (error) {
    throw new Error('Error calling reasonSetNotify-reasonSetNotify: ', error);
  }
};

const taskDeleteNotify = async (data) => {
  const callable = functions.httpsCallable('taskDeleteNotify-taskDeleteNotify');
  try {
    return await callable(data);
  } catch (error) {
    throw new Error('Error calling taskDeleteNotify-taskDeleteNotify: ', error);
  }
};

const taskSetStateNotify = async (data) => {
  const callable = functions.httpsCallable('taskSetStateNotify-taskSetStateNotify');
  try {
    return await callable(data);
  } catch (error) {
    throw new Error('Error calling taskSetStateNotify-taskSetStateNotify: ', error);
  }
};

const taskSetTextNotify = async (data) => {
  const callable = functions.httpsCallable('taskSetTextNotify-taskSetTextNotify');
  try {
    return await callable(data);
  } catch (error) {
    throw new Error('Error calling taskSetTextNotify-taskSetTextNotify: ', error);
  }
};

const remainTasksReminder = async (data) => {
  const callable = functions.httpsCallable('remainTasksReminder-remainTasksReminder');
  try {
    return await callable(data);
  } catch (error) {
    throw new Error('Error calling remainTasksReminder-remainTasksReminder: ', error);
  }
};

export {
  reasonSetNotify,
  taskDeleteNotify,
  taskSetStateNotify,
  taskSetTextNotify,
  remainTasksReminder,
};
