import React, { useState } from 'react'; /* global chrome */

import PropTypes from 'prop-types';

import GuestAvatar from './GuestAvatar.jsx';

const { storage, runtime } = chrome;
const defaultAvatarUrl = runtime.getURL('/images/default-avatar.png');

const MyMeetingsEventAttendee = ({
  email,
  responseStatus,
}) => {
  const [display, setDisplay] = useState(email);
  const [avatarUrl, setAvatarUrl] = useState(defaultAvatarUrl);

  const key = `meetings.ensembl.so/guests/${email}`;
  storage.local.get([key], (result) => {
    if (result[key]) {
      setDisplay(result[key].display);
      setAvatarUrl(result[key].avatarUrl);
    }
  });

  return (
    <GuestAvatar
      avatarUrl={avatarUrl}
      avatarSize="20px"
      display={display}
      responseStatus={responseStatus}
      hasTooltip
    />
  );
};

MyMeetingsEventAttendee.propTypes = {
  email: PropTypes.string.isRequired,
  responseStatus: PropTypes.string.isRequired,
};

export default MyMeetingsEventAttendee;
