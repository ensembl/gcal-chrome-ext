import React, { useState, useEffect, useContext } from 'react';

import PropTypes from 'prop-types';

import { AuthContext } from './AuthProvider.jsx';
import { FirestoreRefsContext } from './FirestoreRefsProvider.jsx';
import { GcalGuestContext } from './GcalGuestProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { GcalEventContext } from './GcalEventProvider.jsx';
import { setTaskText, setTaskState, deleteTask } from './guestFirestore';
import { taskSetStateNotify, taskDeleteNotify, taskSetTextNotify } from './guestCallables';
import { getNotifyEmails } from './getNotifyEmails';
import TaskView from './TaskView.jsx';
import TaskEdit from './TaskEdit.jsx';
import TaskStateChange from './TaskStateChange.jsx';

const Task = ({
  createdAt,
  text,
  state,
  tasksLeftCount,
}) => {
  const { authUserName, authUserEmail } = useContext(AuthContext);
  const { guestsRef } = useContext(FirestoreRefsContext);
  const { email, display } = useContext(GcalGuestContext);
  const { title, htmlLink } = useContext(GcalEventContext);
  const { orgEmail } = useContext(GcalGuestsContext);

  const [changeStateMode, setChangeStateMode] = useState(false);

  const [textState, setTextState] = useState(text);
  const [editMode, setEditMode] = useState(false);
  const [waitingFirestore, setWaitingFirestore] = useState(false);

  const enterEditMode = () => {
    setTextState(text);
    setEditMode(true);
  };

  const exitEditMode = () => { setEditMode(false); };

  const enterChangeStateMode = () => { setChangeStateMode(true); };
  const exitChangeStateMode = () => { setChangeStateMode(false); };

  const setTextToFirestoreAndNotify = async () => {
    const beforeText = text;
    setWaitingFirestore(true);
    await setTaskText({
      guestsRef,
      email,
      createdAt,
      text: textState,
    });
    const { notifyEmails } = getNotifyEmails({
      authUserEmail, email, orgEmail,
    });
    if (notifyEmails.length > 0) {
      await taskSetTextNotify({
        title,
        htmlLink,
        authUserName,
        authUserEmail,
        display,
        notifyEmails,
        beforeText,
        text: textState,
      });
    }
  };

  const setStateToFirestoreAndNotify = async ({ newState }) => {
    const tasksLeftCountBefore = tasksLeftCount;
    await setTaskState(({
      guestsRef,
      email,
      createdAt,
      state: newState,
    }));
    const { notifyEmails } = getNotifyEmails({
      authUserEmail, email, orgEmail,
    });
    if (notifyEmails.length > 0) {
      await taskSetStateNotify({
        title,
        htmlLink,
        authUserName,
        authUserEmail,
        display,
        notifyEmails,
        text,
        state: newState,
        tasksLeftCount: tasksLeftCountBefore + (newState === 'pending action' ? 1 : -1),
      });
    }
  };

  const deleteFromFirestoreAndNotify = async () => {
    await deleteTask({
      guestsRef,
      email,
      createdAt,
    });
    const { notifyEmails } = getNotifyEmails({
      authUserEmail, email, orgEmail,
    });
    await taskDeleteNotify({
      title,
      htmlLink,
      authUserName,
      authUserEmail,
      display,
      notifyEmails,
      text,
    });
  };

  useEffect(() => {
    setWaitingFirestore(false);
  }, [text]);

  return (
    <>
      {!editMode && (
        <TaskView
          text={waitingFirestore ? textState : text}
          state={state}
          waitingFirestore={waitingFirestore}
          enterEditMode={enterEditMode}
          changeStateMode={changeStateMode}
          enterChangeStateMode={enterChangeStateMode}
        />
      )}
      {editMode && (
        <TaskEdit
          textState={textState}
          setTextState={setTextState}
          text={text}
          setTextToFirestoreAndNotify={setTextToFirestoreAndNotify}
          deleteFromFirestoreAndNotify={deleteFromFirestoreAndNotify}
          exitEditMode={exitEditMode}
        />
      )}
      {changeStateMode && (
        <TaskStateChange
          state={state}
          exitChangeStateMode={exitChangeStateMode}
          setStateToFirestoreAndNotify={setStateToFirestoreAndNotify}
        />
      )}
    </>
  );
};

Task.propTypes = {
  createdAt: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  setText: PropTypes.func.isRequired,
  state: PropTypes.string.isRequired,
  tasksLeftCount: PropTypes.number.isRequired,
};

export default Task;
