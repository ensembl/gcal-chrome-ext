import React from 'react';

import PropTypes from 'prop-types';
import {
  Box, Button, CircularProgress, Stack,
} from '@mui/material';

const BlockOffPrepare = ({
  redirectToPrep,
  visitingPrep,
  blockOffLength,
  startTimeString,
}) => {
  const length = `${blockOffLength} minutes`;
  const date = new Date(startTimeString).toLocaleDateString('en-US', { weekday: 'short', month: 'short', day: 'numeric' });
  const time = new Date(startTimeString).toLocaleTimeString('en-US', { hour12: true, hour: 'numeric', minute: 'numeric' });

  return (
    <Box marginTop="10px">
      <Box>
        {'Blocked off '}
        <Box component="span" fontWeight="bold">{length}</Box>
        {' on '}
        <Box component="span" fontWeight="bold">{date}</Box>
        {' at '}
        <Box component="span" fontWeight="bold">{time}</Box>
        .
      </Box>
      <Button
        style={{ paddingLeft: 0, paddingRight: 0 }}
        onClick={redirectToPrep}
        color="primary"
        disabled={visitingPrep}
      >
        {visitingPrep
          ? (
            <Stack direction="row" alignItems="center" justifyContent="center">
              <CircularProgress color="inherit" size={14} />
              <Box marginLeft="7px">View block</Box>
            </Stack>
          )
          : 'View block'
        }
      </Button>
    </Box>
  );
};

BlockOffPrepare.propTypes = {
  redirectToPrep: PropTypes.func.isRequired,
  visitingPrep: PropTypes.bool.isRequired,
  blockOffLength: PropTypes.number.isRequired,
  startTimeString: PropTypes.string.isRequired,
};

export default BlockOffPrepare;
