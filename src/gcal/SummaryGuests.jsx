import React, { useContext } from 'react';

import PropTypes from 'prop-types';

import { Stack } from '@mui/material';

import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { GcalGuestProvider } from './GcalGuestProvider.jsx';
import SummaryGuest from './SummaryGuest.jsx';
import { getTasksData } from './getTasksData';

const isFinished = (guest) => {
  if (!guest.tasks) { return true; }
  const { checkedAll } = getTasksData({ tasks: guest.tasks });
  return checkedAll;
};

const SummaryGuests = ({ guests, finished }) => {
  const { orgEmail, gcalGuests } = useContext(GcalGuestsContext);

  return (
    <Stack
      padding='5px 15px'
      width="50%"
      flexWrap="wrap"
      direction="row"
      justifyContent="space-evenly"
    >
      {gcalGuests
        .filter((gcalGuest) => gcalGuest.email !== orgEmail)
        .map((gcalGuest) => {
          const guest = guests.find((g) => g.email === gcalGuest.email);
          const showGuest = finished
            ? !guest || (guest && isFinished(guest))
            : guest && !isFinished(guest);
          return showGuest
            ? (
              <GcalGuestProvider
                key={gcalGuest.email}
                gcalGuest={gcalGuest}
              >
                <SummaryGuest
                  tasks={(guest && guest.tasks) || {}}
                />
              </GcalGuestProvider>
            ) : <></>;
        })
      }
    </Stack>
  );
};

SummaryGuests.propTypes = {
  guests: PropTypes.array.isRequired,
  finished: PropTypes.bool,
};

SummaryGuests.defaultProps = {
  finished: false,
};

export default SummaryGuests;
