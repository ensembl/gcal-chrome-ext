import React from 'react';

import Linkify from 'linkify-react';
import { Box } from '@mui/material';
import PropTypes from 'prop-types';

const TaskViewText = ({
  text,
}) => (
  <Box
    component="span"
    className="EnsemblDontBreakOut"
  >
    <Linkify options={{ target: '_blank' }}>
      {text}
    </Linkify>
  </Box>
);

TaskViewText.propTypes = {
  text: PropTypes.string.isRequired,
};

export default TaskViewText;
