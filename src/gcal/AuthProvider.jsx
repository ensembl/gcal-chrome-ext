import React from 'react';

import PropTypes from 'prop-types';

export const AuthContext = React.createContext();

export const AuthProvider = ({
  authUser,
  children,
}) => (
  <AuthContext.Provider
    value={{
      authUid: authUser.uid || '',
      authUserEmail: authUser.email || '',
      authUserName: authUser.displayName || '',
    }}
  >
    {children}
  </AuthContext.Provider>
);

AuthProvider.propTypes = {
  authUser: PropTypes.objectOf(PropTypes.any).isRequired,
  children: PropTypes.node.isRequired,
};

export default AuthProvider;
