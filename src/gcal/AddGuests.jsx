import React, { useState, useEffect, useRef } from 'react';

import {
  Autocomplete, Avatar, Box, Card, CardHeader, TextField, Paper,
} from '@mui/material';

import {
  getGuestsInput, getGuestsLists, getInvalidGuestsInput,
} from './htmlNodes';

import './AddGuests.css';

const { runtime } = chrome; /* global chrome */

const defaultAvatarUrl = runtime.getURL('/images/default-avatar.png');

const AddGuests = () => {
  const [guestsOption, setGuestsOption] = useState([]);
  const [guestState, setGuestState] = useState('');
  const [initialRender, setInitialRender] = useState(true);

  const inputRef = useRef();

  const updateGcalGuestsInput = ({ value }) => {
    // Don't touch gcal input if the ensembl input is empty
    if (value) {
      const { guestsInput } = getGuestsInput();
      if (guestsInput) {
        guestsInput.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true }));
        guestsInput.value = value;
        guestsInput.dispatchEvent(new KeyboardEvent('keyup', { bubbles: true }));
        guestsInput.dispatchEvent(new Event('input', { bubbles: true, cancelable: true }));
      }
    }
  };

  const selectGcalGuestDiv = ({ id }) => {
    const guestDiv = document.querySelector(`div[data-hovercard-id="${id}"]`);
    if (guestDiv) {
      guestDiv.dispatchEvent(new KeyboardEvent('keydown'), { key: 'Enter', bubbles: true });
      guestDiv.dispatchEvent(new KeyboardEvent('keyup', { key: 'Enter', bubbles: true }));
      guestDiv.dispatchEvent(new MouseEvent('click'), { bubbles: true });
      setGuestState('');
    }
  };

  const clearGcalGuestsInput = () => {
    const { guestsInput } = getGuestsInput();
    if (guestsInput) {
      setTimeout(() => {
        guestsInput.focus();
        guestsInput.blur();
        guestsInput.value = '';
        inputRef.current.focus();
      });
    }
  };

  const createOptionsFromGuestNodes = ({ guestNodes }) => Array.from(guestNodes).map(
    (node) => {
      const img = node.querySelector('img');
      const { innerText } = node;
      const [name, id] = innerText.split('\n');
      return {
        id, label: name, value: id, avatarUrl: img.getAttribute('src'),
      };
    },
  );

  const findGuestNodes = () => {
    let guestNodes;
    let { guestsLists } = getGuestsLists();
    // Sometimes the document return two of the same element but one is empty
    if (!guestsLists) { return { guestNodes }; }
    guestsLists = Array.from(guestsLists).filter(
      (guestList) => guestList.children.length > 0,
    );
    const [guestsList] = guestsLists;
    if (guestsList) {
      guestNodes = guestsList.children;
    }
    return { guestNodes };
  };

  const onInputChange = (event, value) => {
    if (event) {
      updateGcalGuestsInput({ value });
      const { guestNodes } = findGuestNodes();
      setGuestState(value);
      if (guestNodes) {
        setGuestsOption(createOptionsFromGuestNodes({ guestNodes }));
      }
    }
  };

  const onChange = (event, value) => {
    if (value) {
      setGuestState(value.id);
      selectGcalGuestDiv({ id: value.id });
    }
  };

  const onKeyDown = (event) => {
    if (event.code === 'Enter') {
      const { guestsInput } = getGuestsInput();
      if (guestsInput) {
        guestsInput.focus();
        guestsInput.dispatchEvent(new KeyboardEvent('keydown', { key: 'Enter', bubbles: true }));
        guestsInput.dispatchEvent(new Event('input', { key: 'Enter', bubbles: true, cancelable: true }));
        guestsInput.dispatchEvent(new KeyboardEvent('keyup', { key: 'Enter', bubbles: true }));
      }

      const { invalidGuestsInput } = getInvalidGuestsInput();
      if (!invalidGuestsInput) {
        setGuestState('');
      }
    }
  };

  useEffect(() => {
    if (guestState === '' && !initialRender) {
      clearGcalGuestsInput();
    }
    setInitialRender(false);
  }, [guestState]);

  const customPaperComponent = (params) => (
    <Paper
      {...params}
      style={{ fontSize: '0.9rem' }}
    />
  );

  const customInput = (params) => (
    <TextField
      {...params}
      inputProps={{
        ...params.inputProps,
        style: { fontSize: '0.9rem' },
      }}
      inputRef={inputRef}
      onKeyDown={onKeyDown}
      placeholder="Add guests"
    />);

  const customRenderOption = (props, option) => (
    <Card
      {...props}
      margin="0"
      paddingX="10px"
      style={{
        boxShadow: 'none', borderRadius: 0,
      }}
    >
      <CardHeader
        style={{ margin: 0, padding: 0 }}
        avatar={
          <Avatar
            style={{ height: '28px', width: '28px' }}
            src={option.avatarUrl || defaultAvatarUrl}
          />
        }
        title={<Box fontSize="0.875rem">{option.value}</Box>}
        subheader={<Box fontSize="0.75rem">{option.id}</Box>}
      />
    </Card >
  );

  return (
    <Autocomplete
      className="EnsemblAddGuests"
      disablePortal
      disableClearable
      onChange={onChange}
      onInputChange={onInputChange}
      options={guestsOption}
      size="small"
      fullWidth
      value={guestState}
      noOptionsText={'Type name or email address'}
      inputValue={guestState}
      PaperComponent={customPaperComponent}
      renderInput={customInput}
      renderOption={customRenderOption}
    />
  );
};

export default AddGuests;
