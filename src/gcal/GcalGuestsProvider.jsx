import React from 'react';

import PropTypes from 'prop-types';

export const GcalGuestsContext = React.createContext();

export const GcalGuestsProvider = ({
  gcalGuests,
  children,
}) => {
  let orgEmail = '';
  let orgDisplay = '';
  let orgAvatarUrl = '';
  const organizerGuest = gcalGuests.find((gcalGuest) => gcalGuest.organizer);
  if (organizerGuest) {
    ({ email: orgEmail, display: orgDisplay, avatarUrl: orgAvatarUrl } = organizerGuest);
  }

  return (
    <GcalGuestsContext.Provider
      value={{
        gcalGuests,
        orgEmail,
        orgDisplay,
        orgAvatarUrl,
      }}
    >
      {children}
    </GcalGuestsContext.Provider>
  );
};

GcalGuestsProvider.propTypes = {
  gcalGuests: PropTypes.array.isRequired,
  children: PropTypes.node.isRequired,
};

export default GcalGuestsProvider;
