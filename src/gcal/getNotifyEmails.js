const getNotifyEmails = ({
  authUserEmail, email, orgEmail,
}) => {
  const notifyEmails = [];
  // Notify guest if you are not that guest
  if (authUserEmail !== email) {
    notifyEmails.push(email);
  }
  // Notify organizer if you are not the organizer (and wasn't just added in previous if)
  if (authUserEmail !== orgEmail && orgEmail !== email) {
    notifyEmails.push(orgEmail);
  }
  return { notifyEmails };
};

export { getNotifyEmails };
