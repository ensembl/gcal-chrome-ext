const getGcalDiv = () => ({ gcalDiv: document.querySelector('div.pSp5K') });

const getCreateButton = () => ({ createButton: document.querySelector('button[aria-label="Create"]') });

const getModalViewEventDiv = () => ({ modalViewEventDiv: document.querySelector('#xDetDlg') });

const getModalViewEventCloseDiv = () => ({ modalViewEventCloseDiv: document.querySelector('#xDetDlgCloseBu') });

// Dock to sidebar button that indicates that the create event modal is open
const getDockToSidebarDiv = () => ({ dockToSidebarDiv: document.querySelector('div[data-tooltip="Dock to sidebar"]') });

// Add description link to programmatically click in create event modal
const getAddDescriptionSpan = () => ({ addDescriptionSpan: document.querySelector('span.ahrbyf[data-key="description"') });

// Description field
const getDescriptionDiv = () => ({ descriptionDiv: document.querySelector('div[aria-label="Description"][contenteditable="true"]') });

const getOptionsDiv = () => ({ optionsDiv: document.querySelector('div[aria-label="Options"][data-tooltip="Options"]') });

const getGuestNodes = () => ({ guestNodes: document.querySelectorAll('div[data-hovercard-id][role="treeitem"]') });

const getGuestNodeDisplaySpan = ({ guestNode }) => ({ guestNodeDisplaySpan: guestNode.querySelector('span.cHB8o') });

const getGuestNodeAvatarDiv = ({ guestNode }) => ({ guestNodeAvatarDiv: guestNode.querySelector('div.jPtXgd') });

const getGcalEventIdOnPageEvent = ({ newEvent }) => {
  let eventId;
  const idElement = document.querySelector(`div[jsname="${newEvent ? 'PmeJed' : 'nB7Rvb'}"]`);
  if (idElement) {
    const matchArr = idElement.getAttribute('jslog').match(/2:(\[.*\]);/);
    if (matchArr && matchArr.length && matchArr[1]) {
      [, eventId] = matchArr[1].split('"');
    }
  }
  return { gcalEventId: eventId };
};

const getGcalEventIdOnModalViewEvent = () => {
  let eventId;
  const { modalViewEventDiv } = getModalViewEventDiv();
  if (modalViewEventDiv) {
    [eventId] = window.atob(modalViewEventDiv.getAttribute('data-eventid')).split(' ');
  }
  return { gcalEventId: eventId };
};

const clickMoreOptionsSpan = () => {
  const moreOptionsSpan = document.querySelector('div[jsname="rhPddf"]');
  if (moreOptionsSpan) {
    moreOptionsSpan.click();
  }
};

const getSaveButtonDiv = () => ({ saveButtonDiv: document.querySelector('div[jsname="x8hlje"]') });

const getCreateWithButtonAttached = () => ({ attached: !!document.querySelector('#EnsemblCreateWithButton') });

const getModalViewEventTitleDiv = () => ({ titleDiv: document.querySelector('#xDtlDlgCt > div.kMp0We.OcVpRe.IyS93d.N1DhNb') });

const getOpenButton = () => ({ openButton: document.querySelector('#EnsemblOpenButton') });

const getAvatarDiv = () => ({ avatarDiv: document.querySelector('div[ng-non-bindable][data-ogsr-up]') });

const getOpenIcon = () => ({ openIcon: document.querySelector('#EnsemblOpenIcon') });

const getEditEventDiv = () => ({ editEventDiv: document.querySelector('div[data-tooltip="Edit event"]') });

const getBaseContainer = () => ({ baseContainer: document.querySelector('#EnsemblBaseContainer') });

const getDynamicContainer = () => ({ dynamicContainer: document.querySelector('#EnsemblDynamicContainer') });

const getDynamicContainerAttached = ({ modalViewEventDiv = null } = {}) => {
  if (modalViewEventDiv) {
    return { attached: !!modalViewEventDiv.querySelector('#EnsemblDynamicContainer') };
  }
  return { attached: !!document.querySelector('#EnsemblDynamicContainer') };
};

const getTitleInput = () => ({ titleInput: document.querySelector('input[aria-label="Title"]') });

const getTitleInputValue = () => {
  const { titleInput } = getTitleInput();
  if (titleInput) {
    return { titleInputValue: titleInput.value };
  }
  return { titleInputValue: '' };
};

const getPageNewSaveButtonDiv = () => ({ saveButtonDiv: document.querySelector('div[role="button"][aria-label="Save"]') });

const getPageCancelButtonDiv = () => ({ cancelButtonDiv: document.querySelector('#xCancelBu') });

const getEventChipDiv = ({ dataEventId }) => ({ eventChipDiv: document.querySelector(`div[data-eventchip][data-eventid="${dataEventId}"]`) });

const getUserEmail = () => {
  const userEmailDiv = document.querySelector('#xUserEmail');
  if (userEmailDiv) {
    return { userEmail: userEmailDiv.innerText };
  }
  return { userEmail: '' };
};

const getRecurringOnPage = () => {
  const dropdownSpan = document.querySelector('div[jsname="wQNmvb"][tabindex="0"] > span');
  if (dropdownSpan && dropdownSpan.innerText === 'Does not repeat') {
    return { recurring: false };
  }
  const div = document.querySelector('div.JvkSDc');
  if (div && div.innerText === 'Does not repeat') {
    return { recurring: false };
  }
  return { recurring: true };
};

const getGuestsInput = () => ({ guestsInput: document.querySelector('input[aria-label="Guests"]') });

const getGuestsLists = () => ({ guestsLists: document.querySelectorAll('ul[peoplekit-id="rymPhb"]') });

const getRemoveGuestSpan = ({ email }) => {
  const removeGuestSpan = document
    .querySelector(`div[data-id="${email}"] div[data-tooltip="Remove"] span[aria-hidden="true"]`);
  return { removeGuestSpan };
};

const getInvalidGuestsInput = () => ({ invalidGuestsInput: document.querySelector('input[aria-label="Guests"][aria-invalid="true"]') });

export {
  getGcalDiv,
  getCreateButton,
  getModalViewEventDiv,
  getModalViewEventCloseDiv,
  getDockToSidebarDiv,
  getAddDescriptionSpan,
  getDescriptionDiv,
  getOptionsDiv,
  getGuestNodes,
  getGuestNodeDisplaySpan,
  getGuestNodeAvatarDiv,
  getGcalEventIdOnPageEvent,
  getGcalEventIdOnModalViewEvent,
  clickMoreOptionsSpan,
  getSaveButtonDiv,
  getModalViewEventTitleDiv,
  getCreateWithButtonAttached,
  getOpenButton,
  getOpenIcon,
  getAvatarDiv,
  getEditEventDiv,
  getBaseContainer,
  getDynamicContainer,
  getDynamicContainerAttached,
  getTitleInput,
  getTitleInputValue,
  getPageNewSaveButtonDiv,
  getPageCancelButtonDiv,
  getEventChipDiv,
  getUserEmail,
  getRecurringOnPage,
  getGuestsInput,
  getGuestsLists,
  getRemoveGuestSpan,
  getInvalidGuestsInput,
};
