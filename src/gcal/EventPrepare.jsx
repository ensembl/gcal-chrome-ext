import React, { useContext, useState, useEffect } from 'react';

import {
  Box, Button, CircularProgress, Stack,
} from '@mui/material';
import PropTypes from 'prop-types';

import { setGcalEventInfo } from './setGcalEventInfo';
import { redirectToEvent } from './redirectToEvent';
import { AuthContext } from './AuthProvider.jsx';

const EventPrepare = ({
  origEventId,
}) => {
  const { authUserEmail } = useContext(AuthContext);
  const [visitingOrig, setVisitingOrig] = useState(false);

  const [origTitle, setOrigTitle] = useState('');
  const [origHtmlLink, setOrigHtmlLink] = useState('');

  const redirectToOrig = () => {
    setVisitingOrig(true);
    redirectToEvent({ eventId: origEventId, htmlLink: origHtmlLink, email: authUserEmail });
  };

  useEffect(() => {
    setGcalEventInfo({
      gcalEventId: origEventId,
      setTitle: setOrigTitle,
      setHtmlLink: setOrigHtmlLink,
    });
  }, []);

  return (
    <>
      <h2 style={{ fontSize: '1.6rem', marginTop: 0, marginBottom: 10 }}>
        {`Prepare for: ${origTitle}`}
      </h2>
      <Box>
        {'⏳ Use this time to prepare for '}
        <Box component="span" fontWeight="bold">{origTitle}</Box>
        .
      </Box>
      <Button
        style={{ paddingLeft: 0, paddingRight: 0 }}
        onClick={redirectToOrig}
        color="primary"
        disabled={visitingOrig}
      >
        {visitingOrig
          ? (
            <Stack direction="row" alignItems="center" justifyContent="center">
              <CircularProgress color="inherit" size={14} />
              <Box marginLeft="7px">View meeting</Box>
            </Stack>
          )
          : 'View meeting'
        }
      </Button>
    </>
  );
};

EventPrepare.propTypes = {
  origEventId: PropTypes.string.isRequired,
  origTitle: PropTypes.string.isRequired,
  origHtmlLink: PropTypes.string.isRequired,
};

export default EventPrepare;
