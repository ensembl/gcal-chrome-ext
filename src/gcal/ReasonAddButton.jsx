import React from 'react';

import PropTypes from 'prop-types';
import { Button } from '@mui/material';

const ReasonAddButton = ({
  enterEditMode,
}) => (
  <Button
    style={{ paddingLeft: 0, paddingRight: 0 }}
    onClick={enterEditMode}
    color='primary'
  >
    Add reason
  </Button>
);

ReasonAddButton.propTypes = {
  enterEditMode: PropTypes.string.isRequired,
};

export default ReasonAddButton;
