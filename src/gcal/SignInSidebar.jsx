import React, { useEffect } from 'react';

import { Box, Link, Stack } from '@mui/material';
import PropTypes from 'prop-types';

import { firebase } from '../firebase-config';
import CloseSidebarButton from './CloseSidebarButton.jsx';

const { runtime } = chrome; /* global chrome */
const { credential } = firebase.auth.GoogleAuthProvider;
const auth = firebase.auth();
const buttonUrl = runtime.getURL('/images/google-sign-in.png');
const logoUrl = runtime.getURL('/images/icon-128x128.png');

const SignInSidebar = ({ setAuthLoaded, setAuthUser }) => {
  const signInToFirebase = ({ token }) => {
    auth.signInWithCredential(
      credential(null, token),
    ).catch((error) => {
      console.error('Error signing in with credential', error);
    });
  };

  const onClick = () => {
    setAuthLoaded(false);
    runtime.sendMessage(
      { msg: 'get-chrome-token' },
      (response) => {
        const { token } = response;
        signInToFirebase({ token });
      },
    );
  };

  useEffect(() => {
    auth.onAuthStateChanged((user) => {
      setAuthUser(user);
      setAuthLoaded(true);
    });
  }, []);

  return (
    <Box paddingY="5px" paddingX="20px">
      <Stack direction="row" justifyContent="flex-end">
        <CloseSidebarButton />
      </Stack>
      <Stack marginTop="75px" alignItems="center">
        <img src={logoUrl} height="50px" width="50px" />
        <h2 style={{ fontSize: '1.6rem', margin: 0 }}>
          Ensembl Meeting Prep
        </h2>
        <Box
          style={{ marginTop: '40px', cursor: 'pointer' }}
          onClick={onClick}
        >
          <img
            height="50px"
            src={buttonUrl}
          />
        </Box>
        <Box
          marginTop="10px"
          style={{ textAlign: 'center', fontSize: '0.8rem' }}
        >
          By signing up and signing in to this service you accept our <Link href="https://www.ensembl.so/privacy-policy">privacy policy</Link> and <Link href="https://www.ensembl.so/terms-of-service">terms of service</Link>.
        </Box>
      </Stack>
    </Box>
  );
};

SignInSidebar.propTypes = {
  setAuthLoaded: PropTypes.func.isRequired,
  setAuthUser: PropTypes.func.isRequired,
};

export default SignInSidebar;
