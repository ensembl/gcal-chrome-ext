import React from 'react';

import PropTypes from 'prop-types';
import { Tooltip } from '@mui/material';

const EnsemblTootlip = ({
  placement,
  title,
  content,
}) => (
  <Tooltip
    PopperProps={{ disablePortal: true }}
    placement={placement}
    title={title}
    componentsProps={{
      tooltip: {
        sx: {
          fontSize: '0.9rem',
          fontWeight: 'normal',
          backgroundColor: 'white',
          color: 'black',
          padding: '10px',
          boxShadow: 'rgb(0 0 0 / 20%) 0px 5px 5px -3px, rgb(0 0 0 / 14%) 0px 8px 10px 1px, rgb(0 0 0 / 12%) 0px 3px 14px 2px;',
        },
      },
    }} // https://github.com/mui-org/material-ui/pull/29023
  >
    {content}
  </Tooltip>
);

EnsemblTootlip.propTypes = {
  placement: PropTypes.string.isRequired,
  title: PropTypes.node.isRequired,
  content: PropTypes.node.isRequired,
};

export default EnsemblTootlip;
