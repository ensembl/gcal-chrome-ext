import React from 'react';

import PropTypes from 'prop-types';

export const GcalEventContext = React.createContext();

export const GcalEventProvider = ({
  gcalEventId,
  title,
  titleInputValue,
  htmlLink,
  children,
}) => (
  <GcalEventContext.Provider
    value={{
      gcalEventId,
      title,
      titleInputValue,
      htmlLink,
    }}
  >
    {children}
  </GcalEventContext.Provider>
);

GcalEventProvider.propTypes = {
  gcalEventId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  titleInputValue: PropTypes.string.isRequired,
  htmlLink: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired,
};

export default GcalEventProvider;
