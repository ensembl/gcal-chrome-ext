import React, { useState, useRef } from 'react';

import { Button, Box } from '@mui/material';

import TaskAutoSave from './TaskAutoSave.jsx';

const TaskAutoSaves = () => {
  const [initialCreatedAt] = useState(new Date().getTime());

  // Array of when TaskEdit components are added, but not immediately saved to Firestore yet.
  const [createdAts, setCreatedAts] = useState([initialCreatedAt]);
  const addTaskButtonRef = useRef();

  const onEnterPressed = () => {
    if (addTaskButtonRef) {
      addTaskButtonRef.current.click();
    }
  };

  const deleteCreatedAt = async ({ createdAt }) => {
    setCreatedAts(createdAts.filter((c) => c !== createdAt));
  };

  return (
    <>
      <Box fontWeight="500" marginBottom="5px">
        Tasks
      </Box>
      {createdAts.length > 0 && createdAts.map((createdAt, index) => (
        <Box
          marginBottom="5px"
          key={createdAt}
        >
          <TaskAutoSave
            initialCreatedAt={initialCreatedAt}
            createdAt={createdAt}
            deleteCreatedAt={deleteCreatedAt}
            onEnterPressed={onEnterPressed}
            autoFocus={index === createdAts.length - 1}
          />
        </Box>
      ))}
      <Box marginTop="10px">
        <Button
          onClick={() => {
            setCreatedAts([...createdAts, new Date().getTime()]);
          }}
          color='primary'
          ref={addTaskButtonRef}
        >
          Add task
        </Button>
      </Box>
    </>
  );
};

export default TaskAutoSaves;
