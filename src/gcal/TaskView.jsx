import React from 'react';

import { Box, Stack } from '@mui/material';
import PropTypes from 'prop-types';

import TaskViewStateButton from './TaskViewStateButton.jsx';
import TaskViewText from './TaskViewText.jsx';
import TaskViewEditButton from './TaskViewEditButton.jsx';

import './TaskView.css';

const TaskView = ({
  text,
  state,
  enterEditMode,
  changeStateMode,
  enterChangeStateMode,
}) => (
  <Stack
    className="EnsemblTaskView"
    direction="row"
    alignItems="center"
    justifyContent="space-between"
    padding="3px 0"
  >
    <Stack
      direction="row"
      alignItems="center"
      spacing="5px"
    >
      <TaskViewStateButton
        state={state}
        enterChangeStateMode={enterChangeStateMode}
      />
      <TaskViewText
        text={text}
      />
    </Stack>
    <Box className="EnsemblTaskViewEditButton">
      <TaskViewEditButton
        enterEditMode={enterEditMode}
        disabled={changeStateMode}
      />
    </Box>
  </Stack>
);

TaskView.propTypes = {
  text: PropTypes.string.isRequired,
  state: PropTypes.string.isRequired,
  enterEditMode: PropTypes.string.isRequired,
  changeStateMode: PropTypes.bool.isRequired,
  enterChangeStateMode: PropTypes.func.isRequired,
};

export default TaskView;
