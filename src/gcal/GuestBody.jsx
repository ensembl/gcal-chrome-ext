import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import { AuthContext } from './AuthProvider.jsx';
import { ViewModeContext } from './ViewModeProvider.jsx';
import { GcalGuestContext } from './GcalGuestProvider.jsx';
import Reason from './Reason.jsx';
import Tasks from './Tasks.jsx';
import TaskAutoSaves from './TaskAutoSaves.jsx';
import BlockOff from './BlockOff.jsx';

const GuestBody = ({
  reason,
  reasonInFirestore,
  tasks,
  blockOffStarted,
  prep,
}) => {
  const { autoSave, modalViewEvent } = useContext(ViewModeContext);
  const { email } = useContext(GcalGuestContext);
  const { authUserEmail } = useContext(AuthContext);
  const isGuest = email === authUserEmail;
  const autoBlockOff = isGuest && autoSave;
  const manualBlockOff = isGuest && modalViewEvent;

  return (
    <Box
      padding="5px 15px 15px 15px"
    >
      <Reason
        reason={reason}
        inFirestore={reasonInFirestore}
      />
      <Box marginTop="13px">
        {!autoSave && <Tasks tasks={tasks} />}
        {autoSave && <TaskAutoSaves />}
      </Box>
      {(autoBlockOff || manualBlockOff) && (
        <Box marginTop="10px">
          <BlockOff
            auto={autoBlockOff}
            blockOffStarted={blockOffStarted}
            prep={prep}
          />
        </Box>
      )}
    </Box>
  );
};

GuestBody.propTypes = {
  reason: PropTypes.string.isRequired,
  reasonInFirestore: PropTypes.bool.isRequired,
  tasks: PropTypes.objectOf(PropTypes.any).isRequired,
  blockOffStarted: PropTypes.bool.isRequired,
  prep: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default GuestBody;
