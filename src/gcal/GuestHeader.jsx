import React, { useState, useContext } from 'react';

import pluralize from 'pluralize';
import { Stack, Box } from '@mui/material';
import PropTypes from 'prop-types';
import CloseIcon from '@mui/icons-material/Close';
import CheckIcon from '@mui/icons-material/Check';
import EmailOutlinedIcon from '@mui/icons-material/EmailOutlined';
import MarkedEmailOutlinedIcon from '@mui/icons-material/MarkEmailReadOutlined';

import { reminderSentTooltip } from './reminderSentTooltip';
import { ViewModeContext } from './ViewModeProvider.jsx';
import { GcalGuestContext } from './GcalGuestProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { AuthContext } from './AuthProvider.jsx';
import { GcalEventContext } from './GcalEventProvider.jsx';
import { remainTasksReminder } from './guestCallables';
import { getRemoveGuestSpan } from './htmlNodes';
import { getTasksData } from './getTasksData';
import GuestHeaderIconButton from './GuestHeaderIconButton.jsx';
import GuestAvatar from './GuestAvatar.jsx';

import './GuestHeader.css';

const GuestHeader = ({
  toggleShowBody,
  tasks,
}) => {
  const { autoSave, modalViewEvent } = useContext(ViewModeContext);
  const { authUserName, authUserEmail } = useContext(AuthContext);
  const {
    email, display, avatarUrl, responseStatus,
  } = useContext(GcalGuestContext);
  const { orgDisplay, orgEmail } = useContext(GcalGuestsContext);
  const { title, htmlLink } = useContext(GcalEventContext);
  const isOrganizer = orgEmail === authUserEmail;

  const {
    tasksCount, hasTasks, tasksLeft, tasksLeftCount, checkedAll,
  } = getTasksData({ tasks });

  const [reminderSent, setReminderSent] = useState(false);

  const getTooltip = () => {
    if (!modalViewEvent && isOrganizer) {
      return 'Remove guest';
    }
    if (!hasTasks) {
      return '';
    }
    if (checkedAll) {
      return `${display} completed all ${tasksCount} ${pluralize('task', tasksCount)}`;
    }
    if (!reminderSent) {
      return `Remind ${display} of ${tasksLeftCount} ${pluralize('task', tasksLeftCount)} left`;
    }
    if (reminderSent) {
      return reminderSentTooltip;
    }
    return '';
  };

  const removeGuest = () => {
    const { removeGuestSpan } = getRemoveGuestSpan({ email });
    if (removeGuestSpan) {
      removeGuestSpan.dispatchEvent(new MouseEvent('click', { bubbles: true }));
    }
  };

  const remindGuest = async () => {
    setReminderSent(true);
    try {
      await remainTasksReminder({
        title,
        htmlLink,
        orgDisplay,
        notifyEmail: email,
        authUserName,
        authUserEmail,
        remainingTasksString: JSON.stringify(tasksLeft),
      });
    } catch (error) {
      console.error('Error calling remainTasksReminder', error);
    }
  };

  const onClick = async (e) => {
    e.stopPropagation();
    if (!modalViewEvent) {
      removeGuest();
    } else {
      remindGuest();
    }
  };

  return (
    <Stack
      direction="row"
      alignItems="center"
      onClick={toggleShowBody}
      className="EnsemblGuestHeader"
      style={{ minHeight: '34px' }}
    >
      <GuestAvatar
        avatarUrl={avatarUrl}
        avatarSize="24px"
        display={display}
        responseStatus={responseStatus}
        hasTooltip={!autoSave}
      />
      <Box
        style={{
          fontSize: '1rem',
          textOverflow: 'ellipsis',
          whiteSpace: 'nowrap',
          overflow: 'hidden',
          marginLeft: '5px',
        }}
      >
        {display}
      </Box>
      <Box marginLeft="auto">
        <GuestHeaderIconButton
          getTooltip={getTooltip}
          disabled={modalViewEvent && ((hasTasks && checkedAll) || (!hasTasks) || reminderSent)}
          onClick={onClick}
          icon={
            <>
              {!modalViewEvent && isOrganizer && <CloseIcon className="EnsemblGuestHeader__RemoveGuestBtn" fontSize="small" />}
              {modalViewEvent && hasTasks && checkedAll && <CheckIcon fontSize="small" style={{ color: 'rgb(76, 175, 80)' }} />}
              {modalViewEvent && hasTasks && !checkedAll && !reminderSent && <EmailOutlinedIcon className="EnsemblGuestHeader__EmailBtn" fontSize="small" color="primary" />}
              {modalViewEvent && hasTasks && !checkedAll && reminderSent && <MarkedEmailOutlinedIcon fontSize="small" style={{ color: 'rgb(76, 175, 80)' }} />}
            </>
          }
        />
      </Box>
    </Stack >
  );
};

GuestHeader.propTypes = {
  toggleShowBody: PropTypes.func.isRequired,
  tasks: PropTypes.objectOf(PropTypes.any).isRequired,
};

export default GuestHeader;
