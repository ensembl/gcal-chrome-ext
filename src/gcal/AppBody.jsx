import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import { Box } from '@mui/material';

import { AuthContext } from './AuthProvider.jsx';
import { ViewModeProvider } from './ViewModeProvider.jsx';
import SwitchAccount from './SwitchAccount.jsx';
import Event from './Event.jsx';
import MyMeetings from './MyMeetings.jsx';
import './App.css';

const AppBody = ({
  gcalEventId,
  flatGcalGuests,
  autoSave,
  modalViewEvent,
  modalNewEvent,
  calendarView,
  titleInputValue,
  userHtmlEmail,
  pageViewRecurring,
  setEventReady,
  setMyMeetingsReady,
}) => {
  const { authUserEmail } = useContext(AuthContext);

  return (
    <Box paddingY="10px" paddingX="20px">
      {authUserEmail !== userHtmlEmail && (
        <SwitchAccount
          userHtmlEmail={userHtmlEmail}
        />
      )}
      {authUserEmail === userHtmlEmail && (
        <ViewModeProvider
          autoSave={autoSave}
          modalViewEvent={modalViewEvent}
          modalNewEvent={modalNewEvent}
        >

          {!calendarView && (
            <Event
              gcalEventId={gcalEventId}
              flatGcalGuests={flatGcalGuests}
              titleInputValue={titleInputValue}
              pageViewRecurring={pageViewRecurring}
              setEventReady={setEventReady}
            />

          )}
          {calendarView && (
            <MyMeetings
              setMyMeetingsReady={setMyMeetingsReady}
            />
          )}
        </ViewModeProvider>
      )}
    </Box >
  );
};

AppBody.propTypes = {
  gcalEventId: PropTypes.string.isRequired,
  flatGcalGuests: PropTypes.string.isRequired,
  autoSave: PropTypes.bool.isRequired,
  modalViewEvent: PropTypes.bool.isRequired,
  modalNewEvent: PropTypes.bool.isRequired,
  calendarView: PropTypes.bool.isRequired,
  titleInputValue: PropTypes.string.isRequired,
  userHtmlEmail: PropTypes.string.isRequired,
  pageViewRecurring: PropTypes.bool.isRequired,
  setEventReady: PropTypes.func.isRequired,
  setMyMeetingsReady: PropTypes.func.isRequired,
};

export default AppBody;
