/* global chrome */

import { createGcalObserver } from './createGcalObserver';
import { getGcalDiv } from './htmlNodes';
import { createElement } from './createElement';

const { element: baseContainer } = createElement({
  tag: 'div',
  attributes: [{ key: 'id', value: 'EnsemblBaseContainer' }],
  classNames: ['EnsemblContainer', 'EnsemblHidden'],
});
const { gcalDiv } = getGcalDiv();
if (gcalDiv) {
  gcalDiv.after(baseContainer);
}

const { element: dynamicContainer } = createElement({
  tag: 'div',
  attributes: [{ key: 'id', value: 'EnsemblDynamicContainer' }],
  classNames: ['EnsemblContainer', 'EnsemblHidden'],
});

const { element: createWithButton } = createElement({
  tag: 'button',
  innerText: 'Create with Ensembl',
  attributes: [{ key: 'id', value: 'EnsemblCreateWithButton' }],
  classNames: ['EnsemblButton'],
});

const { element: openButton } = createElement({
  tag: 'button',
  innerText: 'Open Ensembl sidebar',
  attributes: [{ key: 'id', value: 'EnsemblOpenButton' }],
  classNames: ['EnsemblButton'],
});

const { element: openIcon } = createElement({
  tag: 'div',
  attributes: [
    { key: 'id', value: 'EnsemblOpenIcon' },
    { key: 'title', value: 'Open Ensembl sidebar' },
  ],
});
const iconUrl = chrome.runtime.getURL('/images/icon-48x48.png');
const { element: openIconImage } = createElement({
  tag: 'img',
  attributes: [
    { key: 'src', value: iconUrl },
  ],
});
openIcon.appendChild(openIconImage);

createGcalObserver({
  dynamicContainer,
  createWithButton,
  openButton,
  openIcon,
});
