import React from 'react';

import { Box } from '@mui/material';
import PropTypes from 'prop-types';

const MyMeetingsEventDateTime = ({
  startDateDisplay,
  startTimeDisplay,
  endDateDisplay,
  endTimeDisplay,
}) => {
  const sameDay = startDateDisplay === endDateDisplay;
  const dayOnly = sameDay && !startTimeDisplay && !endTimeDisplay;
  const start = `${sameDay && !dayOnly ? '' : startDateDisplay} ${startTimeDisplay}`;
  const dash = (endDateDisplay !== startDateDisplay) || endTimeDisplay ? '-' : '';
  const end = `${sameDay ? '' : endDateDisplay} ${endTimeDisplay}`;

  return (
    <Box
      fontWeight="500"
      fontSize="0.8rem"
    >
      {`${start} ${dash} ${end}`}
    </Box>
  );
};

MyMeetingsEventDateTime.propTypes = {
  startDateDisplay: PropTypes.string.isRequired,
  startTimeDisplay: PropTypes.string.isRequired,
  endDateDisplay: PropTypes.string.isRequired,
  endTimeDisplay: PropTypes.string.isRequired,
};

export default MyMeetingsEventDateTime;
