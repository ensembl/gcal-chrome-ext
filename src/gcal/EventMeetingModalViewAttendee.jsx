import React, { useContext } from 'react';

import PropTypes from 'prop-types';

import { AuthContext } from './AuthProvider.jsx';
import { GcalGuestsContext } from './GcalGuestsProvider.jsx';
import { GcalGuestProvider } from './GcalGuestProvider.jsx';
import Attendee from './Attendee.jsx';

const EventMeetingModalViewAttendee = ({
  guests,
}) => {
  const { gcalGuests } = useContext(GcalGuestsContext);
  const { authUserEmail } = useContext(AuthContext);

  const authGcalGuest = gcalGuests.find((g) => g.email === authUserEmail);
  // authGuest may be undefined if it doesn't exist yet in Firestore
  const authGuest = guests.find((g) => g.email === authUserEmail);

  return (
    <GcalGuestProvider
      gcalGuest={authGcalGuest}
    >
      <Attendee
        reason={(authGuest && authGuest.reason) || ''}
        tasks={(authGuest && authGuest.tasks) || {}}
        blockOffStarted={!!authGuest && !!authGuest.blockOffStarted}
        prep={(authGuest && authGuest.prep) || {}}
      />
    </GcalGuestProvider>
  );
};

EventMeetingModalViewAttendee.propTypes = {
  guests: PropTypes.array.isRequired,
};

export default EventMeetingModalViewAttendee;
