import React from 'react';

import { Box, Stack } from '@mui/material';
import PropTypes from 'prop-types';

import SignOut from './SignOut.jsx';
import CloseSidebarButton from './CloseSidebarButton.jsx';
import './AppHeader.css';

const AppHeader = ({
  displayName,
  email,
  avatarUrl,
  setEventReady,
  setMyMeetingsReady,
}) => (
  <Box className="EnsemblAppHeader">
    <Stack
      direction="row"
      alignItems="center"
      justifyContent="space-between"
    >
      <SignOut
        displayName={displayName}
        email={email}
        avatarUrl={avatarUrl}
        setEventReady={setEventReady}
        setMyMeetingsReady={setMyMeetingsReady}
      />
      <CloseSidebarButton />
    </Stack>
  </Box>
);

AppHeader.propTypes = {
  displayName: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  avatarUrl: PropTypes.string.isRequired,
  setEventReady: PropTypes.func.isRequired,
  setMyMeetingsReady: PropTypes.func.isRequired,
};

export default AppHeader;
