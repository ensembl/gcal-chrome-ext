import React from 'react';

import { Stack, Alert } from '@mui/material';
import HelpOutlineIcon from '@mui/icons-material/HelpOutline';

import EnsemblTootlip from './EnsemblTooltip.jsx';
import './RecurringWarning.css';

const RecurringWarning = () => (
  <Alert
    icon={false}
    severity="info"
    style={{ border: '1px solid grey', padding: 0 }}
    className="EnsemblRecurringWarning"
  >
    <Stack direction="row" alignItems="center" spacing="3px">
      <span>Recurring meeting instance</span>
      <EnsemblTootlip
        placement="top"
        title="Changes to reasons, tasks, and likes are saved to this meeting instance only"
        content={<HelpOutlineIcon fontSize="small" />}
      />
    </Stack>
  </Alert>
);

export default RecurringWarning;
