import React, {
  useState, useEffect, useContext, useRef,
} from 'react';

import PropTypes from 'prop-types';
import { TextField, Card } from '@mui/material';
import { ViewModeContext } from './ViewModeProvider.jsx';
import { GcalEventContext } from './GcalEventProvider.jsx';
import { getTitleInput } from './htmlNodes';

const TitleBodyEdit = ({ setTitleChanged }) => {
  const { autoSave } = useContext(ViewModeContext);
  // External title is changing if user types in title input field
  const { titleInputValue } = useContext(GcalEventContext);
  const externalTitle = titleInputValue === '(No title)' ? '' : titleInputValue;

  const [titleState, setTitleState] = useState(externalTitle);
  const [initialTitle] = useState(externalTitle);

  const inputRef = useRef();

  const { titleInput } = getTitleInput();

  const updateTitleInput = ({ value }) => {
    if (titleInput) {
      titleInput.dispatchEvent(new KeyboardEvent('keydown', { bubbles: true }));
      titleInput.value = value;
      titleInput.dispatchEvent(new KeyboardEvent('keyup', { bubbles: true }));
      titleInput.dispatchEvent(new Event('input', { bubbles: true, cancelable: true }));
    }
  };

  const onChange = (e) => {
    const { value } = e.target;
    setTitleState(value);
    updateTitleInput({ value });
    setTitleChanged(value !== initialTitle);
  };

  useEffect(() => {
    setTitleChanged(externalTitle !== initialTitle);
    if (externalTitle !== titleState) {
      setTitleState(externalTitle);
    }
  }, [externalTitle]);

  return (
    <Card
      variant="outlined"
      style={{ padding: '15px' }}
    >
      <TextField
        fullWidth
        size="small"
        value={titleState}
        onChange={onChange}
        style={{ background: 'white' }}
        variant="standard"
        placeholder="Meeting title, e.g. Project planning"
        inputProps={{ style: { fontSize: '0.9rem' } }}
        inputRef={inputRef}
        autoFocus={autoSave}
      />
    </Card>
  );
};

TitleBodyEdit.propTypes = {
  setTitleChanged: PropTypes.func.isRequired,
};

export default TitleBodyEdit;
