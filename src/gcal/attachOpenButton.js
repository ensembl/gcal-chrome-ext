/* eslint-disable no-param-reassign */

import { getOpenButton, getModalViewEventTitleDiv } from './htmlNodes';
import { getSidebarShow, setSidebarShow, setContainerVisibility } from './sidebar';

const attachOpenButton = ({ openButton }) => {
  const { openButton: attachedOpenButton } = getOpenButton();
  if (!attachedOpenButton) {
    const { titleDiv } = getModalViewEventTitleDiv();
    if (titleDiv) {
      openButton.addEventListener('click', () => {
        setSidebarShow({ show: true });
        setContainerVisibility({ visibility: 'visible' });
      });
      const { show } = getSidebarShow();
      openButton.disabled = show;
      titleDiv.after(openButton);
    }
  }
};

export { attachOpenButton };
