import React, { useContext } from 'react';

import PropTypes from 'prop-types';
import {
  Autocomplete, Box, Card, Paper, TextField,
} from '@mui/material';

import { reasonSuggestions } from './reasonSuggestions';
import { ViewModeContext } from './ViewModeProvider.jsx';
import EditButtons from './EditButtons.jsx';
import './ReasonEdit.css';

const ReasonEdit = ({
  reasonState,
  setReasonState,
  reason,
  setToFirestoreAndNotify,
  exitEditMode,
}) => {
  const { autoSave, modalViewEvent } = useContext(ViewModeContext);

  const customInput = (params) => (
    <TextField
      {...params}
      variant="standard"
      inputProps={{
        ...params.inputProps,
        style: { fontSize: '0.9rem' },
      }}
      InputLabelProps={{ ...params.InputLabelProps, shrink: false }}
      placeholder="Type to enter custom reason"
      autoFocus={!autoSave}
    />
  );

  const onInputChange = (event, value) => {
    setReasonState(value);
  };

  return (
    <Card
      className={`EnsemblReasonEdit__${autoSave ? 'autoSave' : ''}`}
      variant={autoSave ? 'elevation' : 'outlined'}
      elevation="0"
      style={{ padding: `${autoSave ? '0' : '10px 7px'}` }}
    >
      <Autocomplete
        freeSolo
        openOnFocus
        forcePopupIcon
        options={reasonSuggestions}
        onInputChange={onInputChange}
        size="small"
        value={reasonState}
        inputValue={reasonState}
        fullWidth
        disablePortal
        PaperComponent={(params) => (
          <Paper
            {...params}
            style={{
              marginLeft: modalViewEvent ? '7px' : '0',
              fontSize: '0.9rem',
            }}
          />
        )}
        renderInput={customInput}
      />
      {!autoSave && (
        <Box marginTop="10px">
          <EditButtons
            saveDisabled={reasonState === reason}
            clickSave={setToFirestoreAndNotify}
            exitEditMode={exitEditMode}
          />
        </Box>
      )}
    </Card>
  );
};

ReasonEdit.propTypes = {
  reasonState: PropTypes.string.isRequired,
  setReasonState: PropTypes.func.isRequired,
  reason: PropTypes.string.isRequired,
  setToFirestoreAndNotify: PropTypes.func.isRequired,
  exitEditMode: PropTypes.func.isRequired,
};

export default ReasonEdit;
