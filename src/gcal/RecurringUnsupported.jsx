import React from 'react';

import { Alert, AlertTitle, Box } from '@mui/material';

const RecurringUnsupported = () => (
  <Alert
    icon={false}
    severity="info"
    style={{ border: '1px solid grey', fontSize: '0.9rem' }}
  >
    <AlertTitle>Creating recurring meetings unsupported</AlertTitle>
    <Box marginBottom="10px">
      Ensembl does not support creating recurring meetings right now.
    </Box>
    <Box>
      After creating this Google event, visit each individual meeting
      instance and update it with Ensembl.
    </Box>
  </Alert>
);

export default RecurringUnsupported;
