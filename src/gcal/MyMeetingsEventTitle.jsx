import React, { useState, useContext } from 'react';

import {
  Button, Stack, CircularProgress, Box,
} from '@mui/material';
import PropTypes from 'prop-types';

import { AuthContext } from './AuthProvider.jsx';
import { redirectToEvent } from './redirectToEvent';

const MyMeetingsEventTitle = ({
  eventId,
  title,
  htmlLink,
}) => {
  const { authUserEmail } = useContext(AuthContext);

  const [waiting, setWaiting] = useState(false);

  const onClick = () => {
    setWaiting(true);
    redirectToEvent({ eventId, htmlLink, email: authUserEmail });
  };

  return (
    <Button
      style={{
        paddingLeft: 0, paddingRight: 0, fontSize: '0.9rem', textAlign: 'left', lineHeight: '1rem',
      }}
      onClick={onClick}
      color="primary"
      disabled={waiting}
    >
      {waiting
        ? (
          <Stack direction="row" alignItems="center" justifyContent="center">
            <CircularProgress color="inherit" size={14} />
            <Box marginLeft="7px">{title}</Box>
          </Stack>
        )
        : `${title}`
      }
    </Button>
  );
};

MyMeetingsEventTitle.propTypes = {
  eventId: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  htmlLink: PropTypes.string.isRequired,
};

export default MyMeetingsEventTitle;
