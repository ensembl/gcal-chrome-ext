import React, { useEffect, useState, useContext } from 'react';

import PropTypes from 'prop-types';
import { useDocumentData, useCollectionData } from 'react-firebase-hooks/firestore';

import { db } from '../firebase-config';
import { AuthContext } from './AuthProvider.jsx';
import { ViewModeContext } from './ViewModeProvider.jsx';
import { addEventIfNeeded, setGcalGuestEmails } from './eventFirestore';
import { setGcalEventInfo } from './setGcalEventInfo';
import { insertPoweredByEnsembl } from './insertPoweredByEnsembl';
import BackButton from './BackButton.jsx';
import EventMeeting from './EventMeeting.jsx';
import EventPrepare from './EventPrepare.jsx';

const Event = ({
  gcalEventId,
  flatGcalGuests,
  titleInputValue,
  pageViewRecurring,
  setEventReady,
}) => {
  const { authUserEmail, authUid, authUserName } = useContext(AuthContext);
  const { autoSave, modalViewEvent } = useContext(ViewModeContext);

  const eventsRef = db.collection('events');
  const eventRef = gcalEventId ? eventsRef.doc(gcalEventId) : null;
  const guestsRef = eventRef ? eventRef.collection('guests') : null;

  const [event, eventLoading] = useDocumentData(eventRef);
  const [guests, guestsLoading] = useCollectionData(guestsRef, { idField: 'email' });
  const [gcalEventLoading, setGcalEventLoading] = useState(true);
  const [addingEvent, setAddingEvent] = useState(false);
  const [gcalGuests, setGcalGuests] = useState([]);

  const [title, setTitle] = useState('');
  const [htmlLink, setHtmlLink] = useState('');
  const [recurringEventId, setRecurringEventId] = useState('');

  useEffect(() => {
    setEventReady(
      (!eventLoading && !guestsLoading && autoSave)
      || (!eventLoading && !guestsLoading && !autoSave && !gcalEventLoading),
    );
  }, [eventLoading, guestsLoading, autoSave, gcalEventLoading]);

  useEffect(async () => {
    if (eventRef && !addingEvent) {
      setAddingEvent(true);
      await addEventIfNeeded({
        eventRef,
        createdByUid: authUid,
        createdByEmail: authUserEmail,
        createdByDomain: authUserEmail.slice(authUserEmail.indexOf('@') + 1),
        authUserName,
      });
      if (!autoSave) {
        setGcalEventInfo({
          gcalEventId,
          setTitle,
          setHtmlLink,
          setRecurringEventId,
          setGcalEventLoading,
          eventRef,
        });
      }
      setAddingEvent(false);
    }
  }, [gcalEventId]);

  useEffect(async () => {
    const newGcalGuests = JSON.parse(flatGcalGuests);
    setGcalGuests(newGcalGuests);
  }, [flatGcalGuests]);

  useEffect(async () => {
    if (event) {
      await setGcalGuestEmails({
        eventRef,
        gcalGuestEmails: Array.from(
          new Set([
            ...JSON.parse(flatGcalGuests).map((g) => g.email),
            authUserEmail,
          ]),
        ),
      });
    }
  }, [
    flatGcalGuests,
    // When event is done loading (and we've checked it exists) we're ready to write gcalGuestEmails
    eventLoading,
  ]);

  useEffect(async () => {
    if (!eventLoading) {
      if (autoSave) { insertPoweredByEnsembl(); }
    }
  }, [eventLoading]);

  return (
    <>
      <BackButton />
      {event && !event.isPrep && guests && (
        <EventMeeting
          eventsRef={eventsRef}
          gcalEventId={gcalEventId}
          eventRef={eventRef}
          guestsRef={guestsRef}
          gcalGuests={gcalGuests}
          title={title}
          titleInputValue={titleInputValue}
          htmlLink={htmlLink}
          recurring={pageViewRecurring || (recurringEventId && modalViewEvent)}
          guests={guests}
        />
      )}
      {event && event.isPrep && (
        <EventPrepare
          origEventId={event.origEventId || ''}
        />
      )}
    </>
  );
};

Event.propTypes = {
  gcalEventId: PropTypes.string.isRequired,
  flatGcalGuests: PropTypes.string.isRequired,
  titleInputValue: PropTypes.string.isRequired,
  pageViewRecurring: PropTypes.bool.isRequired,
  setEventReady: PropTypes.func.isRequired,
};

export default Event;
