import React, { useState } from 'react';

import {
  Select, MenuItem, FormControl,
} from '@mui/material';

const { storage } = chrome; /* global chrome */

const BlockOffAutoBody = () => {
  const key = 'meetings.ensembl.so/block-off-length';

  const [blockOffLength, setBlockOffLength] = useState('');
  if (!blockOffLength) {
    storage.local.get([key], (result) => {
      if (result[key]) {
        setBlockOffLength(result[key]);
      }
    });
  }

  const onChange = (e) => {
    const { value } = e.target;
    setBlockOffLength(value);
    storage.local.set({
      [key]: value,
    });
  };

  const options = [
    {
      value: 'none',
      display: 'Don\'t block off time',
    },
    {
      value: '15',
      display: 'Block off 15 mins before',
    },
    {
      value: '30',
      display: 'Block off 30 mins before',
    },
  ];

  return (
    <FormControl
      size="small"
      fullWidth
    >
      <Select
        style={{ fontSize: '0.9rem' }}
        value={blockOffLength}
        onChange={onChange}
      >
        {options.map((option) => (
          <MenuItem
            key={option.state}
            value={option.value}
            style={{ fontSize: '0.9rem' }}
          >
            {option.display}
          </MenuItem>
        ))}
      </Select>
    </FormControl>
  );
};

export default BlockOffAutoBody;
