import { Calendar } from './Calendar';

const getGcalEvents = async ({ token }) => {
  const calendar = new Calendar({ token });
  const { result } = await calendar.getEvents();
  return { result };
};

export { getGcalEvents };
