class Calendar {
  constructor({ token }) {
    this.token = token;
    this.insertEventUrl = 'https://www.googleapis.com/calendar/v3/calendars/primary/events/';
    this.freebusyUrl = 'https://www.googleapis.com/calendar/v3/freeBusy';
  }

  getConfig({ method, body }) {
    return {
      method,
      async: true,
      headers: {
        Authorization: `Bearer ${this.token}`,
        'Content-Type': 'application/json',
      },
      contentType: 'json',
      body: JSON.stringify(body),
    };
  }

  async insertEvent({
    title, startTime, endTime, description,
  }) {
    let result;
    const config = this.getConfig({
      method: 'POST',
      body: {
        summary: title,
        start: {
          dateTime: startTime.toISOString(),
        },
        end: {
          dateTime: endTime.toISOString(),
        },
        description,
      },
    });

    try {
      const response = await fetch(this.insertEventUrl, config);
      result = await response.json();
    } catch (error) {
      console.error('unable to create prep event', error);
    }
    return { result };
  }

  // Get rather person is free during the time entered
  async getFreeBusy({
    email, timeMax, timeMin,
  }) {
    const config = this.getConfig({
      method: 'POST',
      body: {
        timeMin: timeMin.toISOString(),
        timeMax: timeMax.toISOString(),
        items: [
          { id: email },
        ],
      },
    });
    const result = await (await fetch(this.freebusyUrl, config)).json();
    return { result };
  }

  async getEvent({
    eventId,
  }) {
    const config = this.getConfig({
      method: 'GET',
    });
    const getEventUrl = `https://www.googleapis.com/calendar/v3/calendars/primary/events/${eventId}?timeZone=UTC`;
    const result = await (await fetch(getEventUrl, config)).json();
    return { result };
  }

  async getEvents() {
    const config = this.getConfig({
      method: 'GET',
    });
    const nowDate = new Date().toISOString();
    const getEventsUrl = `https://www.googleapis.com/calendar/v3/calendars/primary/events?orderBy=startTime&singleEvents=true&timeMin=${nowDate}&maxResults=100&timeZone=UTC`;
    const json = await (await fetch(getEventsUrl, config)).json();
    const { items: events } = json;
    return { result: { events } };
  }
}

export { Calendar };
