import firebase from 'firebase/app';
import 'firebase/firestore';
import 'firebase/functions';
import 'firebase/auth';

const config = {
  apiKey: 'AIzaSyApsrWU0YbVl3Aj5dbHO6EbU73lyv9-vCI',
  databaseURL: 'https://1:1019436963217:web:cf8df61cfde64eed1fb630.firebaseio.com',
  storageBucket: 'meeting-prep--workspace.appspot.com',
  authDomain: 'meeting-prep--workspace.firebaseapp.com',
  projectId: 'meeting-prep--workspace',
  messagingSenderId: '1019436963217',
  appId: '1:1019436963217:web:cf8df61cfde64eed1fb630',
};

firebase.initializeApp(config);

const functions = firebase.functions();
const db = firebase.firestore();

// db.useEmulator('localhost', 8080);
// functions.useEmulator('localhost', 5001);

export {
  db, functions, firebase,
};
