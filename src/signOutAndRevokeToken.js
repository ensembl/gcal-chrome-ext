const { identity } = chrome; /* global chrome */

const signOutAndRevokeToken = async ({ token }) => {
  await fetch('https://oauth2.googleapis.com/revoke', {
    method: 'POST',
    body: new URLSearchParams({ token }),
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });
  identity.clearAllCachedAuthTokens(() => { });
};
export { signOutAndRevokeToken };
