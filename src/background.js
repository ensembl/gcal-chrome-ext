import { getGcalEvent } from './gcal/getGcalEvent';
import { getGcalEvents } from './gcal/getGcalEvents';
import { createPrepGcalEvent } from './gcal/createPrepGcalEvent';
import { signOutAndRevokeToken } from './signOutAndRevokeToken';

const {
  runtime, identity, storage,
} = chrome; /* global chrome */

runtime.onMessage.addListener((request, sender, sendResponse) => {
  const { msg, payload = null } = request;

  identity.getAuthToken({ interactive: true }, async (token) => {
    let response;
    switch (msg) {
      case 'sign-out-and-revoke-token':
        await signOutAndRevokeToken({ token });
        break;
      case 'get-chrome-token':
        response = { token, payload };
        break;
      case 'get-gcal-event':
        ({ result: response } = await getGcalEvent({ token, ...payload }));
        break;
      case 'get-gcal-events':
        ({ result: response } = await getGcalEvents({ token }));
        break;
      case 'create-prep-gcal-event':
        ({ response } = await createPrepGcalEvent({ token, ...payload }));
        break;
      default:
        break;
    }
    sendResponse(response);
  });
  return true;
});

runtime.onInstalled.addListener(() => {
  const key = 'meetings.ensembl.so/block-off-length';
  storage.local.set({
    [key]: '15',
  });
});
