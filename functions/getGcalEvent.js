const axios = require('axios');

const getGcalEvent = async ({ gcalEventId, token }) => {
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  const url = `https://www.googleapis.com/calendar/v3/calendars/primary/events/${gcalEventId}?timeZone=UTC`;
  try {
    const response = await axios.get(url, config);
    const { data } = response;
    return { gcalEvent: data };
  } catch (error) {
    return { gcalEvent: null };
  }
};

module.exports = { getGcalEvent };
