const functions = require('firebase-functions');

const { installExtensionHtml } = require('./installExtensionHtml');
const { addMailNotify } = require('./addMailNotify');

exports.taskDeleteNotify = functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    functions.logger.error('taskDeleteNotify called without auth');
    return;
  }

  const { uid } = context.auth;
  functions.logger.info('taskDeleteNotify', { uid, data });

  const {
    title,
    htmlLink,
    authUserName,
    authUserEmail,
    display,
    notifyEmails,
    text,
  } = data;

  await addMailNotify({
    bccEmails: notifyEmails,
    senderName: authUserName,
    senderEmail: authUserEmail,
    subject: `${title} | ${display}'s task deleted by ${authUserName}`,
    html: `
      <p>
        In <a href="${htmlLink}">${title}</a>, ${authUserName} <b>deleted</b> ${display}'s task:
        <br/>
        <b>${text}</b>
      </p>
      <p>
        Reply to this email to follow up with ${authUserName}.
      </p>
      ${installExtensionHtml}
    `,
  });
});
