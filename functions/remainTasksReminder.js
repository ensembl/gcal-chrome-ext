const functions = require('firebase-functions');

const { installExtensionHtml } = require('./installExtensionHtml');
const { addMailNotify } = require('./addMailNotify');

exports.remainTasksReminder = functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    functions.logger.error('remainTasksReminder called without auth');
    return;
  }

  const { uid } = context.auth;
  functions.logger.info('remainTasksReminder', { uid, data });

  const {
    title,
    htmlLink,
    orgDisplay,
    authUserName,
    authUserEmail,
    notifyEmail,
    remainingTasksString,
  } = data;

  const tasksLeft = JSON.parse(remainingTasksString);
  const tasksLeftCount = tasksLeft.length;

  await addMailNotify({
    bccEmails: notifyEmail,
    senderName: authUserName,
    senderEmail: authUserEmail,
    subject: `${title} | Complete ${tasksLeftCount} remaining task${tasksLeftCount === 1 ? '' : 's'} assigned by ${orgDisplay}`,
    html: `
      <p>
        Complete ${tasksLeftCount} remaining task${tasksLeftCount === 1 ? '' : 's'} in <a href="${htmlLink}">${title}</a> assigned by ${orgDisplay}:
        <br/>
        <ol>
          ${tasksLeft.map((task) => `<li>${task.text}</li>`).join('')}
        </ol>
      </p>
      ${installExtensionHtml}
    `,
  });
});
