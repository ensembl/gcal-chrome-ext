const installExtensionHtml = (
  `
    <div>
      <p>--</p>
      <p style="color: #130d06">
        Complete your tasks with
        <a href="https://chrome.google.com/webstore/detail/ensembl-meeting-prep/mibamnjffhmadmdfoidoakkceppinbpa" target="_blank">Ensembl</a>.
      </p>
      <a href="https://chrome.google.com/webstore/detail/ensembl-meeting-prep/mibamnjffhmadmdfoidoakkceppinbpa" target="_blank">
        <img height="80px" src="https://storage.googleapis.com/web-dev-uploads/image/WlD8wC6g8khYWPJUsQceQkhXSlv1/YT2Grfi9vEBa2wAPzhWa.png"/>
      </a>
    </div>
  `
);

module.exports = { installExtensionHtml };
