const functions = require('firebase-functions');
const axios = require('axios');

const getFreeBusy = async ({
  email, timeMax, timeMin, token,
}) => {
  const freebusyUrl = 'https://www.googleapis.com/calendar/v3/freeBusy';
  const body = {
    timeMin: timeMin.toISOString(),
    timeMax: timeMax.toISOString(),
    items: [
      { id: email },
    ],
  };
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  try {
    const response = await axios.post(freebusyUrl, body, config);
    const { data } = response;
    return { data };
  } catch (error) {
    functions.logger.error('Error getting Freebusy', error);
    return { data: null };
  }
};

module.exports = { getFreeBusy };
