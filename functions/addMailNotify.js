const { admin } = require('./admin');

const db = admin.firestore();

const addMailNotify = async ({
  bccEmails,
  senderName,
  senderEmail,
  subject,
  html,
}) => {
  await db.collection('mail').add({
    createdAt: new Date(),
    bcc: bccEmails,
    from: `${senderName} - Ensembl <ensembl@mg.ensembl.so>`,
    replyTo: `${senderName} <${senderEmail}>`,
    message: {
      subject,
      html,
    },
  });
};

module.exports = { addMailNotify };
