const functions = require('firebase-functions');

const { installExtensionHtml } = require('./installExtensionHtml');
const { addMailNotify } = require('./addMailNotify');

const getStateDisplay = ({ state }) => {
  switch (state) {
    case 'pending action':
      return 'Pending action';
    case 'done':
      return 'Task done';
    case 'not understand':
      return 'I don\'t understand this task';
    case 'discuss meeting':
      return 'Prefer to discuss in meeting';
    default:
      return '';
  }
};

exports.taskSetStateNotify = functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    functions.logger.error('taskSetStateNotify called without auth');
    return;
  }

  const { uid } = context.auth;
  functions.logger.info('taskSetStateNotify', { uid, data });

  const {
    title,
    htmlLink,
    authUserName,
    authUserEmail,
    display,
    notifyEmails,
    text,
    state,
    tasksLeftCount,
  } = data;

  const stateDisplay = getStateDisplay({ state });

  const subjectMessage = tasksLeftCount === 0
    ? `${display} completed all tasks`
    : `${display}'s task state changed to '${stateDisplay}' by ${authUserName}`;

  const tasksLeftMessage = tasksLeftCount === 0
    ? `${display} <b>completed all tasks</b>.`
    : `${display} has <b>${tasksLeftCount} task${tasksLeftCount > 1 ? 's' : ''} left.</b>`;

  await addMailNotify({
    bccEmails: notifyEmails,
    senderName: authUserName,
    senderEmail: authUserEmail,
    subject: `${title} | ${subjectMessage}`,
    html: `
      <p>
        In <a href="${htmlLink}">${title}</a>, ${authUserName} changed ${display}'s task state to:
        <br/>
        <b>${stateDisplay}</b>
      </p>
      <p>
        The task is:
        <br/>
        <b>${text}</b>
      </p>
      <p>
        ${tasksLeftMessage}
      </p>
      <p>
        Reply to this email to follow up with ${authUserName}.
      </p>
      ${installExtensionHtml}
    `,
  });
});
