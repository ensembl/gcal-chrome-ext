const functions = require('firebase-functions');

const { installExtensionHtml } = require('./installExtensionHtml');
const { addMailNotify } = require('./addMailNotify');

exports.taskSetTextNotify = functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    functions.logger.error('taskSetTextNotify called without auth');
    return;
  }

  const { uid } = context.auth;
  functions.logger.info('taskSetTextNotify', { uid, data });

  const {
    title,
    htmlLink,
    authUserName,
    authUserEmail,
    display,
    notifyEmails,
    beforeText,
    text,
  } = data;

  const subject = beforeText
    ? `${title} | ${display}'s task changed by ${authUserName}`
    : `${title} | New task for ${display} created by ${authUserName}`;

  const message = beforeText
    ? `
      <p>
        In <a href="${htmlLink}">${title}</a>, ${authUserName} changed ${display}'s task to:
        <br/>
        <b>${text}</b>
      </p>
      <p>
        It was previously:
        <br/>
        <b>${beforeText}</b>
      </p>
    `
    : `
      <p>
        In <a href="${htmlLink}">${title}</a>, ${authUserName} created a new task for ${display}:
        <br/>
        <b>${text}</b>
      </p>
    `;

  await addMailNotify({
    bccEmails: notifyEmails,
    senderName: authUserName,
    senderEmail: authUserEmail,
    subject,
    html: `
      ${message}
      <p>
        Reply to this email to follow up with ${authUserName}.
      </p>
      ${installExtensionHtml} 
    `,
  });
});
