const functions = require('firebase-functions');
const firestore = require('@google-cloud/firestore');

const client = new firestore.v1.FirestoreAdminClient();
const BUCKET_NAME = 'meeting-prep-backup';
// Leave collectionIds empty to export all collections
// or set to a list of collection IDs to export,
// collectionIds: ['events', 'mail']
// empty array to export all collection;
const collectionIds = ['events'];

const bucket = `gs://${BUCKET_NAME}/${new Date().toISOString()}-hourly`;
exports.backupFirestoreHourly = functions.pubsub
  .schedule('55 * * * *')
  .onRun(() => {
    functions.logger.info('backupFirestoreHourly fired: ',
      { env: process.env, start_date: new Date() });

    const projectId = process.env.GCP_PROJECT || process.env.GCLOUD_PROJECT;
    const databaseName = client.databasePath(projectId, '(default)');

    return client.exportDocuments({
      name: databaseName,
      outputUriPrefix: bucket,
      collectionIds,
    })
      .then((responses) => {
        const response = responses[0];
        functions.logger.info(`Operation Name: ${response.name}`);
      })
      .catch((err) => {
        functions.logger.error('Export operation failed', err);
      });
  });
