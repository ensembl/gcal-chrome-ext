const functions = require('firebase-functions');
const { admin } = require('./admin');

const { getGcalEvent } = require('./getGcalEvent');
const { installExtensionHtml } = require('./installExtensionHtml');
const { addMailNotify } = require('./addMailNotify');

const db = admin.firestore();

exports.eventCreatedNotify = functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    functions.logger.error('eventCreatedNotify called without auth');
    return;
  }

  const { uid } = context.auth;
  functions.logger.info('eventCreatedNotify', { uid, data });

  const { gcalEventId, token } = data;
  const { gcalEvent } = await getGcalEvent({ gcalEventId, token });
  if (!gcalEvent) {
    functions.logger.info('Gcal event not found', { gcalEventId });
    return;
  }

  const {
    status = null,
    htmlLink,
    summary: title = '(No title)',
    attendees = [],
    recurrence = [],
  } = gcalEvent;

  if (
    (status !== 'confirmed') // If user clicks undo, status is not confirmed
    || (recurrence.length > 0)
    || attendees.length === 0
  ) {
    return;
  }

  const gcalGuestEmails = attendees.map((a) => a.email);
  const organizerEmail = attendees.find((a) => a.organizer).email;

  const eventDoc = await db.collection('events').doc(gcalEventId).get();
  const { createdByName = 'Ensembl' } = eventDoc.data();

  const snapshot = await db.collection(`events/${gcalEventId}/guests`).get();

  const messages = [];
  snapshot.forEach((guestDoc) => {
    const email = guestDoc.id;
    if (
      gcalGuestEmails.includes(email)
      && email !== organizerEmail
    ) {
      const guest = guestDoc.data();
      const { reason = '', tasks = {} } = guest;

      const tasksCount = Object.keys(tasks).length;
      const subjectMessage = tasksCount > 0
        ? `Complete ${tasksCount} tasks assigned by ${createdByName}`
        : `${createdByName} invites you to a meeting`;

      const reasonHtml = `
        <p>
          ${createdByName} invites you to attend <a href="${htmlLink}">${title}</a> to:
          <br/>
          <b>${reason || '<i>None</i>'}</b>
        </p>
      `;
      let tasksHtml = '';
      if (tasksCount > 0) {
        Object.entries(tasks)
          .sort((a, b) => parseInt(a[0], 10) - parseInt(b[0], 10))
          .forEach(([, task]) => {
            tasksHtml = `${tasksHtml}<li>${task.text}</li>`;
          });
        tasksHtml = `
        <p>
          Complete ${tasksCount} task${tasksCount > 1 ? 's' : ''} assigned by ${createdByName}:
          <br/>
          <ol>${tasksHtml}</ol>
        </p>
      `;
      }

      messages.push({
        email,
        subjectMessage,
        html: `${reasonHtml}${tasksHtml}`,
      });
    }
  });

  // eslint-disable-next-line no-restricted-syntax
  for (const message of messages) {
    // eslint-disable-next-line no-await-in-loop
    await addMailNotify({
      bccEmails: [message.email],
      senderName: createdByName,
      senderEmail: organizerEmail,
      subject: `${title} | ${message.subjectMessage}`,
      html: `
        ${message.html}
        ${installExtensionHtml}
      `,
    });
  }
});
