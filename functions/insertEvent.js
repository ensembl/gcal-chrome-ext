const axios = require('axios');
const functions = require('firebase-functions');

const insertEvent = async ({
  title, startTime, endTime, description, token,
}) => {
  const insertEventUrl = 'https://www.googleapis.com/calendar/v3/calendars/primary/events/';

  const body = {
    summary: title,
    start: {
      dateTime: startTime.toISOString(),
    },
    end: {
      dateTime: endTime.toISOString(),
    },
    description,
  };
  const config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  try {
    const response = await axios.post(insertEventUrl, body, config);
    const { data } = response;
    return { data };
  } catch (error) {
    functions.logger.error('unable to create prep event', error);
    return { data: null };
  }
};

module.exports = { insertEvent };
