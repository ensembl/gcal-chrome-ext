const functions = require('firebase-functions');

const { installExtensionHtml } = require('./installExtensionHtml');
const { addMailNotify } = require('./addMailNotify');

exports.reasonSetNotify = functions.https.onCall(async (data, context) => {
  if (!context.auth) {
    functions.logger.error('reasonSetNotify called without auth');
    return;
  }

  const { uid } = context.auth;
  functions.logger.info('reasonSetNotify', { uid, data });

  const {
    title,
    htmlLink,
    authUserName,
    authUserEmail,
    display,
    notifyEmails,
    beforeReason,
    reason,
  } = data;

  const none = '<i>None</i>';
  await addMailNotify({
    bccEmails: notifyEmails,
    senderName: authUserName,
    senderEmail: authUserEmail,
    subject: `${title} | ${display}'s reason changed by ${authUserName}`,
    html: `
      <p>
        In <a href="${htmlLink}">${title}</a>, ${authUserName} changed ${display}'s reason to:
        <br/>
        <b>${reason || none}</b>
      </p>
      <p>
        It was previously:
        <br/>
        <b>${beforeReason || none}</b>
      </p>
      <p>
        Reply to this email to follow up with ${authUserName}.
      </p>
      ${installExtensionHtml}
    `,
  });
});
